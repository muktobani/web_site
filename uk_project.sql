-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 06, 2016 at 12:31 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `uk_project`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_info`
--

CREATE TABLE IF NOT EXISTS `admin_info` (
  `admin_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(100) NOT NULL,
  `user_name` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `photo` varchar(100) NOT NULL,
  `insert_time` timestamp NOT NULL,
  `insert_by` int(2) NOT NULL,
  `update_time` timestamp NOT NULL,
  `update_by` int(2) NOT NULL,
  `is_active` int(2) NOT NULL,
  PRIMARY KEY (`admin_id`),
  UNIQUE KEY `admin_id` (`admin_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `admin_info`
--

INSERT INTO `admin_info` (`admin_id`, `user_id`, `user_name`, `password`, `photo`, `insert_time`, `insert_by`, `update_time`, `update_by`, `is_active`) VALUES
(4, 'admin', 'admin', '21232f297a57a5a743894a0e4a801fc3', '1467800514_Banglavoice.jpg', '2016-07-05 22:21:54', 1, '0000-00-00 00:00:00', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `ad_banner`
--

CREATE TABLE IF NOT EXISTS `ad_banner` (
  `ad_banner_id` int(11) NOT NULL AUTO_INCREMENT,
  `view_page` int(2) NOT NULL,
  `ad_position` int(2) NOT NULL,
  `banner_photo` varchar(100) NOT NULL,
  `is_active` int(2) NOT NULL,
  `insert_time` timestamp NOT NULL,
  `insert_by` int(5) NOT NULL,
  `update_time` timestamp NOT NULL,
  `update_by` int(5) NOT NULL,
  PRIMARY KEY (`ad_banner_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `ad_banner`
--

INSERT INTO `ad_banner` (`ad_banner_id`, `view_page`, `ad_position`, `banner_photo`, `is_active`, `insert_time`, `insert_by`, `update_time`, `update_by`) VALUES
(2, 1, 1, '1467732294_Chrysanthemum.jpg', 0, '2016-07-05 03:24:54', 1, '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `article`
--

CREATE TABLE IF NOT EXISTS `article` (
  `article_id` int(11) NOT NULL AUTO_INCREMENT,
  `article` longtext NOT NULL,
  `title` varchar(256) NOT NULL,
  `category_id` int(11) NOT NULL,
  `like` int(11) NOT NULL,
  `dis_like` int(11) NOT NULL,
  `hit` varchar(100) NOT NULL,
  `photo` varchar(500) NOT NULL,
  `video` varchar(500) NOT NULL,
  `tag` varchar(500) NOT NULL,
  `is_active` int(2) NOT NULL,
  `insert_time` timestamp NOT NULL,
  `insert_by` int(5) NOT NULL,
  `update_time` timestamp NOT NULL,
  `update_by` int(5) NOT NULL,
  PRIMARY KEY (`article_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `article`
--

INSERT INTO `article` (`article_id`, `article`, `title`, `category_id`, `like`, `dis_like`, `hit`, `photo`, `video`, `tag`, `is_active`, `insert_time`, `insert_by`, `update_time`, `update_by`) VALUES
(1, '<p>ss</p>\r\n', 'ddd', 1, 0, 0, '', '1467705791_Chrysanthemum.jpg', '', '', 0, '2016-07-04 20:03:11', 1, '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `directories`
--

CREATE TABLE IF NOT EXISTS `directories` (
  `directories_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_logo` varchar(100) NOT NULL,
  `company_name` varchar(100) NOT NULL,
  `desceription` text NOT NULL,
  `web_address` varchar(100) NOT NULL,
  `email_address` varchar(100) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `business_category` int(5) NOT NULL,
  `facebook` varchar(100) NOT NULL,
  `google_plus` int(100) NOT NULL,
  `linkedin` int(100) NOT NULL,
  `twitter` int(100) NOT NULL,
  `gallery` varchar(500) NOT NULL,
  `insert_time` timestamp NOT NULL,
  `insert_by` int(3) NOT NULL,
  `is_active` int(3) NOT NULL,
  PRIMARY KEY (`directories_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `lib_album`
--

CREATE TABLE IF NOT EXISTS `lib_album` (
  `album_id` int(11) NOT NULL AUTO_INCREMENT,
  `album_name` varchar(100) NOT NULL,
  `description` varchar(500) NOT NULL,
  `album_cover_photo` varchar(1000) NOT NULL,
  `is_active` int(2) NOT NULL,
  `insert_time` timestamp NOT NULL,
  `insert_by` int(5) NOT NULL,
  `update_time` timestamp NOT NULL,
  `update_by` int(5) NOT NULL,
  PRIMARY KEY (`album_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `lib_album`
--

INSERT INTO `lib_album` (`album_id`, `album_name`, `description`, `album_cover_photo`, `is_active`, `insert_time`, `insert_by`, `update_time`, `update_by`) VALUES
(2, 'Naturla', 'dds', '1467624202_Chrysanthemum.jpg', 0, '2016-07-03 21:23:22', 1, '2016-07-04 19:47:10', 1),
(3, 'Graphic', 'dds', '1467624825_Desert.jpg', 0, '2016-07-03 21:25:06', 1, '2016-07-04 19:47:25', 1);

-- --------------------------------------------------------

--
-- Table structure for table `lib_country`
--

CREATE TABLE IF NOT EXISTS `lib_country` (
  `country_id` int(11) NOT NULL AUTO_INCREMENT,
  `country_full_name` varchar(100) NOT NULL,
  `country_short_name` varchar(100) NOT NULL,
  `flag` varchar(100) NOT NULL,
  `country_code` varchar(100) NOT NULL,
  `currency` varchar(50) NOT NULL,
  `is_active` int(2) NOT NULL,
  `insert_time` timestamp NOT NULL,
  `insert_by` int(5) NOT NULL,
  `update_time` timestamp NOT NULL,
  `update_by` int(5) NOT NULL,
  PRIMARY KEY (`country_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `lib_country`
--

INSERT INTO `lib_country` (`country_id`, `country_full_name`, `country_short_name`, `flag`, `country_code`, `currency`, `is_active`, `insert_time`, `insert_by`, `update_time`, `update_by`) VALUES
(1, 'Bangladesh', 'BN', '1467626299_Desert.jpg', '008', 'TK', 0, '2016-07-03 21:46:56', 1, '2016-07-03 22:01:49', 1),
(2, 'zz', 'zzxz', '1467626519_Jellyfish.jpg', 'zxzxz', 'zxzx', 0, '2016-07-03 22:01:59', 1, '0000-00-00 00:00:00', 0),
(3, 'zz', 'zzxz', '1467626674_Jellyfish.jpg', 'zxzxz', 'zxzx', 0, '2016-07-03 22:04:34', 1, '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `lib_gallery`
--

CREATE TABLE IF NOT EXISTS `lib_gallery` (
  `gallery_id` int(11) NOT NULL AUTO_INCREMENT,
  `album_id` int(11) NOT NULL,
  `gallery_title` varchar(100) NOT NULL,
  `description` varchar(500) NOT NULL,
  `photo` varchar(100) NOT NULL,
  `is_active` int(2) NOT NULL,
  `insert_time` timestamp NOT NULL,
  `insert_by` int(5) NOT NULL,
  `update_time` timestamp NOT NULL,
  `update_by` int(5) NOT NULL,
  PRIMARY KEY (`gallery_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `lib_gallery`
--

INSERT INTO `lib_gallery` (`gallery_id`, `album_id`, `gallery_title`, `description`, `photo`, `is_active`, `insert_time`, `insert_by`, `update_time`, `update_by`) VALUES
(1, 2, 'Test ', 'ssdddd', '1467704866_Chrysanthemum.jpg', 0, '2016-07-04 19:47:46', 1, '2016-07-04 19:55:19', 1),
(2, 3, 'Test ', 'ssdddd', '1467704914_Chrysanthemum.jpg', 0, '2016-07-04 19:48:34', 1, '2016-07-04 19:55:27', 1),
(3, 2, 'Test ', 'ssdddd', '1467704962_Chrysanthemum.jpg', 0, '2016-07-04 19:49:22', 1, '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `mail_template`
--

CREATE TABLE IF NOT EXISTS `mail_template` (
  `mail_template_id` int(11) NOT NULL AUTO_INCREMENT,
  `mail_template_name` varchar(50) NOT NULL,
  `mail_subject` varchar(100) NOT NULL,
  `mail_template` text NOT NULL,
  `insert_time` timestamp NOT NULL,
  `insert_by` int(11) NOT NULL,
  `update_time` timestamp NOT NULL,
  `update_by` int(2) NOT NULL,
  `is_active` int(2) NOT NULL,
  PRIMARY KEY (`mail_template_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `mail_template`
--

INSERT INTO `mail_template` (`mail_template_id`, `mail_template_name`, `mail_subject`, `mail_template`, `insert_time`, `insert_by`, `update_time`, `update_by`, `is_active`) VALUES
(1, 'Template 1', 'xxx rrr', '<h2 style="color:Red; font-weight:bold; text-decoration:underline">xxxx&nbsp; dddd</h2>\r\n\r\n<h2 style="color:Red; font-weight:bold; text-decoration:underline">&nbsp;</h2>\r\n', '2016-07-06 06:36:17', 1, '2016-07-06 06:40:51', 1, 1),
(2, 'Template 2', 'xxx', '<p>xxxx</p>\r\n', '2016-07-06 06:38:14', 1, '2016-07-06 06:41:02', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `media_link`
--

CREATE TABLE IF NOT EXISTS `media_link` (
  `media_link_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `link` varchar(100) NOT NULL,
  `link_icon` varchar(1000) NOT NULL,
  `category` int(5) NOT NULL,
  `is_active` int(2) NOT NULL,
  `insert_time` timestamp NOT NULL,
  `insert_by` int(5) NOT NULL,
  `update_time` timestamp NOT NULL,
  `update_by` int(5) NOT NULL,
  PRIMARY KEY (`media_link_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=58 ;

--
-- Dumping data for table `media_link`
--

INSERT INTO `media_link` (`media_link_id`, `title`, `link`, `link_icon`, `category`, `is_active`, `insert_time`, `insert_by`, `update_time`, `update_by`) VALUES
(4, 'ssss', 'sssqqqq', '', 4, 0, '0000-00-00 00:00:00', 1, '0000-00-00 00:00:00', 0),
(9, 'dd', 'dd', '', 1, 0, '0000-00-00 00:00:00', 1, '0000-00-00 00:00:00', 0),
(10, 'SI3CaT51R2U', 'ss', '', 1, 0, '0000-00-00 00:00:00', 1, '0000-00-00 00:00:00', 0),
(11, 'ss', 'ss', '', 1, 0, '0000-00-00 00:00:00', 1, '0000-00-00 00:00:00', 0),
(12, 's', 's', '', 1, 0, '0000-00-00 00:00:00', 1, '0000-00-00 00:00:00', 0),
(13, 'sss', 'ssss', '', 1, 0, '0000-00-00 00:00:00', 1, '0000-00-00 00:00:00', 0),
(14, 'zz', 'zz', '', 1, 0, '0000-00-00 00:00:00', 1, '0000-00-00 00:00:00', 0),
(15, 'dd', 'ddd', '1467544873_Desert.jpg', 1, 0, '0000-00-00 00:00:00', 1, '0000-00-00 00:00:00', 0),
(16, 'saaa', 'aa', '1467545216_Chrysanthemum.jpg', 1, 0, '2016-07-02 23:26:56', 1, '0000-00-00 00:00:00', 0),
(17, 'zz', 'z', '1467545269_Jellyfish.jpg', 1, 0, '2016-07-02 23:27:49', 1, '0000-00-00 00:00:00', 0),
(18, 'qqq', 'q', '1467545310_Desert.jpg', 1, 0, '2016-07-02 23:28:30', 1, '0000-00-00 00:00:00', 0),
(19, 'sss', 'www', '1467545404_Desert.jpg', 1, 0, '2016-07-03 11:30:04', 1, '0000-00-00 00:00:00', 0),
(20, 'sss', 'sss', '1467545498_Chrysanthemum.jpg', 1, 0, '2016-07-02 23:31:38', 1, '0000-00-00 00:00:00', 0),
(21, 'zz', 'zzz', '1467545776_Chrysanthemum.jpg', 1, 0, '2016-07-02 23:36:16', 1, '0000-00-00 00:00:00', 0),
(23, 'qq', 'qq', '', 1, 0, '2016-07-02 23:38:04', 1, '0000-00-00 00:00:00', 0),
(24, 'qq', 'qq', '', 1, 0, '2016-07-02 23:38:50', 1, '0000-00-00 00:00:00', 0),
(25, 'qq', 'qq', '', 1, 0, '2016-07-02 23:39:10', 1, '0000-00-00 00:00:00', 0),
(26, 'qq', 'qq', '', 1, 0, '2016-07-02 23:39:39', 1, '0000-00-00 00:00:00', 0),
(27, 'xx', 'xx', '', 1, 0, '2016-07-02 23:40:07', 1, '0000-00-00 00:00:00', 0),
(28, 'xx', 'xx', '', 1, 0, '2016-07-02 23:51:35', 1, '0000-00-00 00:00:00', 0),
(29, 'xx', 'xx', '', 1, 0, '2016-07-02 23:52:03', 1, '0000-00-00 00:00:00', 0),
(30, 'xx', 'xx', '', 1, 0, '2016-07-02 23:52:50', 1, '0000-00-00 00:00:00', 0),
(31, 'xx', 'xx', '', 1, 0, '2016-07-02 23:53:07', 1, '0000-00-00 00:00:00', 0),
(32, 'xx', 'xx', '', 1, 0, '2016-07-02 23:53:25', 1, '0000-00-00 00:00:00', 0),
(33, 'xx', 'xx', '', 1, 0, '2016-07-02 23:53:42', 1, '0000-00-00 00:00:00', 0),
(34, 'xx', 'xx', '', 1, 0, '2016-07-02 23:54:11', 1, '0000-00-00 00:00:00', 0),
(35, 'xx', 'xx', '', 1, 0, '2016-07-02 23:54:28', 1, '0000-00-00 00:00:00', 0),
(36, 'xx', 'xx', '', 1, 0, '2016-07-02 23:57:29', 1, '0000-00-00 00:00:00', 0),
(37, 'xx', 'xx', '', 1, 0, '2016-07-02 23:58:26', 1, '0000-00-00 00:00:00', 0),
(38, 'xx', 'xx', '', 1, 0, '2016-07-02 23:58:45', 1, '0000-00-00 00:00:00', 0),
(39, 'xx', 'xx', '', 1, 0, '2016-07-02 23:59:22', 1, '0000-00-00 00:00:00', 0),
(40, 'xx', 'xx', '', 1, 0, '2016-07-03 00:00:50', 1, '0000-00-00 00:00:00', 0),
(41, 'xx', 'xx', '', 1, 0, '2016-07-03 00:01:22', 1, '0000-00-00 00:00:00', 0),
(42, 'xx', 'xx', '', 1, 0, '2016-07-03 00:01:52', 1, '0000-00-00 00:00:00', 0),
(43, 'xx', 'xx', '', 1, 0, '2016-07-03 00:02:06', 1, '0000-00-00 00:00:00', 0),
(44, 'xx', 'xx', '', 1, 0, '2016-07-03 00:03:01', 1, '0000-00-00 00:00:00', 0),
(45, 'xx', 'xx', '', 1, 0, '2016-07-03 00:03:16', 1, '0000-00-00 00:00:00', 0),
(46, 'xx', 'xx', '', 1, 0, '2016-07-03 00:03:25', 1, '0000-00-00 00:00:00', 0),
(47, 'xx', 'xx', 'default.gif', 1, 0, '2016-07-03 00:05:20', 1, '0000-00-00 00:00:00', 0),
(48, 'xx', 'xx', 'default.gif', 1, 0, '2016-07-03 00:06:13', 1, '0000-00-00 00:00:00', 0),
(49, 'xx', 'xx', 'default.gif', 1, 0, '2016-07-03 00:06:28', 1, '0000-00-00 00:00:00', 0),
(50, 'xx', 'xx', 'default.gif', 1, 0, '2016-07-03 00:06:39', 1, '0000-00-00 00:00:00', 0),
(51, 'xx', 'xx', 'default.gif', 1, 0, '2016-07-03 00:06:50', 1, '0000-00-00 00:00:00', 0),
(52, 'xx', 'xx', 'default.gif', 1, 0, '2016-07-03 00:08:08', 1, '0000-00-00 00:00:00', 0),
(54, 'xx', 'xx', '1467555394_Tulips.jpg', 2, 0, '2016-07-03 00:21:27', 1, '2016-07-03 02:16:34', 1),
(55, 'xxvvv', 'xx', '1467555232_Hydrangeas.jpg', 2, 0, '2016-07-03 02:05:39', 1, '2016-07-03 02:13:52', 1),
(56, 'xxreza', 'xx', '1467555454_Chrysanthemum.jpg', 2, 0, '2016-07-03 02:05:50', 1, '2016-07-03 02:17:34', 1),
(57, 'xx', 'xx', '1467554761_Chrysanthemum.jpg', 2, 0, '2016-07-03 02:06:01', 1, '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `member_info`
--

CREATE TABLE IF NOT EXISTS `member_info` (
  `member_id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `age` int(3) NOT NULL,
  `address` varchar(256) NOT NULL,
  `city` varchar(50) NOT NULL,
  `state` varchar(50) NOT NULL,
  `zip` varchar(50) NOT NULL,
  `cuntery` int(11) NOT NULL,
  `photo` varchar(100) NOT NULL,
  `biography` varchar(256) NOT NULL,
  `facebook` varchar(50) NOT NULL,
  `google_plus` varchar(50) NOT NULL,
  `linkedin` varchar(50) NOT NULL,
  `twitter` varchar(50) NOT NULL,
  `insert_time` timestamp NOT NULL,
  `insert_by` int(11) NOT NULL,
  `update_time` timestamp NOT NULL,
  `update_by` int(2) NOT NULL,
  `is_active` int(2) NOT NULL,
  PRIMARY KEY (`member_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `member_info`
--

INSERT INTO `member_info` (`member_id`, `email`, `password`, `name`, `phone`, `age`, `address`, `city`, `state`, `zip`, `cuntery`, `photo`, `biography`, `facebook`, `google_plus`, `linkedin`, `twitter`, `insert_time`, `insert_by`, `update_time`, `update_by`, `is_active`) VALUES
(2, 'reza@gmail.com', '2be9bd7a3434f7038ca27d1918de58bd', '', '', 0, '', '', '', '', 1, '1467800110_Lighthouse.jpg', '', '', '', '', '', '2016-07-05 21:46:39', 1, '2016-07-05 22:15:10', 1, 0),
(3, 'admin@ffff.bim', '934b535800b1cba8f96a5d72f72f1611', '', '', 0, '', '', '', '', 1, '1467800093_Koala.jpg', '', '', '', '', '', '2016-07-05 21:55:14', 1, '2016-07-05 22:14:52', 1, 0),
(4, 'admin@ffff.bim', 'b59c67bf196a4758191e42f76670ceba', '', '', 0, '', '', '', '', 1, '1467800016_Desert.jpg', '', '', '', '', '', '2016-07-05 22:11:38', 1, '2016-07-05 22:13:36', 1, 0),
(5, 'admin@gmail.com', '21232f297a57a5a743894a0e4a801fc3', 'Reza', '01552601805', 0, '', '', '', '', 1, '1467800207_Koala.jpg', '', '', '', '', '', '2016-07-05 22:15:49', 1, '2016-07-05 22:17:24', 1, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
