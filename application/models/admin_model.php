<?php

class Admin_model extends  CI_Model  {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
	function get_link_category()
    {
        return array(1=>'Web Link',2=>'Social Link',3=>'Video Link',4=>'Others Link');
    }

	function get_company_category()
    {
        return array(''=>'--Select--',1=>'IT',2=>'Developer');
    }

	function get_status()
    {
        return array(0=>'Draft',1=>'Published');
    }

	function get_article_category()
    {
        return array(1=>'Blog',2=>'About Us',3=>'Contract Us');
    }

	function get_page()
    {
        return array(1=>'Home',2=>'Blog',3=>'About Us',4=>'Contract Us');
    }

	function get_ad_position()
    {
        return array(1=>1,2=>2,3=>3,4=>4,5=>5,6=>6,7=>7,8=>8,9=>9,10=>10);
    }

	function get_timestamp($format="Y-m-d h:i:s a")
    {
        return date($format);
    }

	function get_name_titile()
    {
         return array(''=>'--Select Title--','Mr'=>'Mr','Mrs'=>'Mrs','Ms'=>'Ms');
    }

	function get_blood_group()
    {
         return array(''=>'--Select--','1'=>'A+','2'=>'A-','3'=>'B+','4'=>'B-','5'=>'AB+','6'=>'AB-','7'=>'O+','8'=>'O-');
    }

	function get_yes_no()
    {
         return array('1'=>'Yes','2'=>'No');
    }

	function get_member_type()
    {
         return array(1=>'General Member',2=>'Lifetime Member',3=>'Honorary Member- (Subject ot EC & GM Approval)');
    }


	function get_valid_year_fees()
    {
         $year_frees_arr=$this->getLibArr('select renew_year,fees from lib_renew_year_frees order by renew_year','renew_year','fees');
         $renew_year=$this->get_renew_year();
		 $data_arr=array(''=>'--Please Select Membership Term--');
		 foreach($year_frees_arr as $key=>$value){
			$data_arr[$key]=$renew_year[$key].' - $'.$value;	 
		 }
		 return $data_arr;
    }

	function get_renew_year()
    {
         return array(1=>'1 Year',2=>'2 Years',3=>'3 Years',4=>'4 Years',5=>'5 Years',6=>'6 Years',7=>'7 Years',8=>'8 Years',9=>'9 Years',10=>'10 Years',20=>'Lifetime');
    }



	function get_countery()
    {
        return array(
					0=>"--Select--",
					1=>"Afghanistan",
					2=>"Aland Islands",
					3=>"Albania",
					4=>"Algeria",
					5=>"American Samoa",
					6=>"Andorra",
					7=>"Angola",
					8=>"Anguilla",
					9=>"Antarctica",
					10=>"Antigua and Barbuda",
					11=>"Argentina",
					12=>"Armenia",
					13=>"Aruba",
					14=>"Australia",
					15=>"Austria",
					16=>"Azerbaijan",
					17=>"Bahamas",
					18=>"Bahrain",
					19=>"Bangladesh",
					20=>"Barbados",
					21=>"Belarus",
					22=>"Belgium",
					23=>"Belize",
					24=>"Benin",
					25=>"Bermuda",
					26=>"Bhutan",
					27=>"Bolivia, Plurinational State of",
					28=>"Bonaire, Sint Eustatius and Saba",
					29=>"Bosnia and Herzegovina",
					30=>"Botswana",
					31=>"Bouvet Island",
					32=>"Brazil",
					33=>"British Indian Ocean Territory",
					34=>"Brunei Darussalam",
					35=>"Bulgaria",
					36=>"Burkina Faso",
					37=>"Burundi",
					38=>"Cambodia",
					39=>"Cameroon",
					40=>"Canada",
					41=>"Cape Verde",
					42=>"Cayman Islands",
					43=>"Central African Republic",
					44=>"Chad",
					45=>"Chile",
					46=>"China",
					47=>"Christmas Island",
					48=>"Cocos array(Keeling) Islands",
					49=>"Colombia",
					50=>"Comoros",
					51=>"Congo",
					52=>"Congo, the Democratic Republic of the",
					53=>"Cook Islands",
					54=>"Costa Rica",
					55=>"Côte d'Ivoire",
					56=>"Croatia",
					57=>"Cuba",
					58=>"Curaçao",
					59=>"Cyprus",
					60=>"Czech Republic",
					61=>"Denmark",
					62=>"Djibouti",
					63=>"Dominica",
					64=>"Dominican Republic",
					65=>"Ecuador",
					66=>"Egypt",
					67=>"El Salvador",
					68=>"Equatorial Guinea",
					69=>"Eritrea",
					70=>"Estonia",
					71=>"Ethiopia",
					72=>"Falkland Islands array(Malvinas)",
					73=>"Faroe Islands",
					74=>"Fiji",
					75=>"Finland",
					76=>"France",
					77=>"French Guiana",
					78=>"French Polynesia",
					79=>"French Southern Territories",
					80=>"Gabon",
					81=>"Gambia",
					82=>"Georgia",
					83=>"Germany",
					84=>"Ghana",
					85=>"Gibraltar",
					86=>"Greece",
					87=>"Greenland",
					88=>"Grenada",
					89=>"Guadeloupe",
					90=>"Guam",
					91=>"Guatemala",
					92=>"Guernsey",
					93=>"Guinea",
					94=>"Guinea-Bissau",
					95=>"Guyana",
					96=>"Haiti",
					97=>"Heard Island and McDonald Islands",
					98=>"Holy See array(Vatican City State)",
					99=>"Honduras",
					100=>"Hong Kong",
					101=>"Hungary",
					102=>"Iceland",
					103=>"India",
					104=>"Indonesia",
					105=>"Iran, Islamic Republic of",
					106=>"Iraq",
					107=>"Ireland",
					108=>"Isle of Man",
					109=>"Israel",
					110=>"Italy",
					111=>"Jamaica",
					112=>"Japan",
					113=>"Jersey",
					114=>"Jordan",
					115=>"Kazakhstan",
					116=>"Kenya",
					117=>"Kiribati",
					118=>"Korea, Democratic People's Republic of",
					119=>"Korea, Republic of",
					120=>"Kuwait",
					121=>"Kyrgyzstan",
					122=>"Lao People's Democratic Republic",
					123=>"Latvia",
					124=>"Lebanon",
					125=>"Lesotho",
					126=>"Liberia",
					127=>"Libya",
					128=>"Liechtenstein",
					129=>"Lithuania",
					130=>"Luxembourg",
					131=>"Macao",
					132=>"Macedonia, The Former Yugoslav Republic of",
					133=>"Madagascar",
					134=>"Malawi",
					135=>"Malaysia",
					136=>"Maldives",
					137=>"Mali",
					138=>"Malta",
					139=>"Marshall Islands",
					140=>"Martinique",
					141=>"Mauritania",
					142=>"Mauritius",
					143=>"Mayotte",
					144=>"Mexico",
					145=>"Micronesia, Federated States of",
					146=>"Moldova, Republic of",
					147=>"Monaco",
					148=>"Mongolia",
					149=>"Montenegro",
					150=>"Montserrat",
					151=>"Morocco",
					152=>"Mozambique",
					153=>"Myanmar",
					154=>"Namibia",
					155=>"Nauru",
					156=>"Nepal",
					157=>"Netherlands",
					158=>"New Caledonia",
					159=>"New Zealand",
					160=>"Nicaragua",
					161=>"Niger",
					162=>"Nigeria",
					163=>"Niue",
					164=>"Norfolk Island",
					165=>"Northern Mariana Islands",
					166=>"Norway",
					167=>"Oman",
					168=>"Pakistan",
					169=>"Palau",
					170=>"Palestine, State of",
					171=>"Panama",
					172=>"Papua New Guinea",
					173=>"Paraguay",
					174=>"Peru",
					175=>"Philippines",
					176=>"Pitcairn",
					177=>"Poland",
					178=>"Portugal",
					179=>"Puerto Rico",
					180=>"Qatar",
					181=>"Réunion",
					182=>"Romania",
					183=>"Russian Federation",
					184=>"Rwanda",
					185=>"Saint Barthélemy",
					186=>"Saint Helena, Ascension and Tristan da Cunha",
					187=>"Saint Kitts and Nevis",
					188=>"Saint Lucia",
					189=>"Saint Martin array(French part)",
					190=>"Saint Pierre and Miquelon",
					191=>"Saint Vincent and the Grenadines",
					192=>"Samoa",
					193=>"San Marino",
					194=>"Sao Tome and Principe",
					195=>"Saudi Arabia",
					196=>"Senegal",
					197=>"Serbia",
					198=>"Seychelles",
					199=>"Sierra Leone",
					200=>"Singapore",
					201=>"Sint Maarten array(Dutch part)",
					202=>"Slovakia",
					203=>"Slovenia",
					204=>"Solomon Islands",
					205=>"Somalia",
					206=>"South Africa",
					207=>"South Georgia and the South Sandwich Islands",
					208=>"South Sudan",
					209=>"Spain",
					210=>"Sri Lanka",
					211=>"Sudan",
					212=>"Suriname",
					213=>"Svalbard and Jan Mayen",
					214=>"Swaziland",
					215=>"Sweden",
					216=>"Switzerland",
					217=>"Syrian Arab Republic",
					218=>"Taiwan, Province of China",
					219=>"Tajikistan",
					220=>"Tanzania, United Republic of",
					221=>"Thailand",
					222=>"Timor-Leste",
					223=>"Togo",
					224=>"Tokelau",
					225=>"Tonga",
					226=>"Trinidad and Tobago",
					227=>"Tunisia",
					228=>"Turkey",
					229=>"Turkmenistan",
					230=>"Turks and Caicos Islands",
					231=>"Tuvalu",
					232=>"Uganda",
					233=>"Ukraine",
					234=>"United Arab Emirates",
					235=>"United Kingdom",
					236=>"United States",
					237=>"United States Minor Outlying Islands",
					238=>"Uruguay",
					239=>"Uzbekistan",
					240=>"Vanuatu",
					241=>"Venezuela, Bolivarian Republic of",
					242=>"Viet Nam",
					243=>"Virgin Islands, British",
					244=>"Virgin Islands, U.S.",
					245=>"Wallis and Futuna",
					246=>"Western Sahara",
					247=>"Yemen",
					248=>"Zambia",
					249=>"Zimbabwe"
				);
    }
	
 	public function get_max_id($table,$column)
	{
		$this->db->select_max($column);
		$query = $this->db->get($table);
		$data_arr = $query->result();
		return ($data_arr[0]->$column)+1;
	}
	
		
	
	//send verification email to user's email id
		function sendEmail($to_email)
		{
			$from_email = 'muktobani@gmail.com'; //change this to yours
			$subject = 'Verify Your Email Address';
			$message = 'Dear User,<br /><br />Please click on the below activation link to verify your email address.<br /><br /> http://www.mydomain.com/user/verify/' . md5($to_email) . '<br /><br /><br />Thanks<br />Mydomain Team';
			
			//configure email settings
			$config['protocol'] = 'smtp';
			$config['smtp_host'] = 'ssl://smtp.linkinbd.com'; //smtp host name
			$config['smtp_port'] = '465'; //smtp port number
			$config['smtp_user'] = $from_email;
			$config['smtp_pass'] = '********'; //$from_email password
			$config['mailtype'] = 'html';
			$config['charset'] = 'utf-8';//iso-8859-1
			$config['wordwrap'] = TRUE;
			$config['newline'] = "\r\n"; //use double quotes
			$this->email->initialize($config);
			
			//send mail
			$this->email->from($from_email, 'Mydomain');
			$this->email->to($to_email);
			$this->email->subject($subject);
			$this->email->message($message);
			return $this->email->send();
		}
		
		
		
	public function upload_files($files,$file_name, $upload_path='uploads/')
    {
        $config = array(
            'upload_path'   => $upload_path,
            'allowed_types' => 'jpg|gif|png|txt|pdf|doc',
            'overwrite'     => 1,                       
        );
        $this->load->library('upload', $config);
        $images = array();

        foreach ($files['name'] as $key => $image) {
            $_FILES['images[]']['name']= $files['name'][$key];
            $_FILES['images[]']['type']= $files['type'][$key];
            $_FILES['images[]']['tmp_name']= $files['tmp_name'][$key];
            $_FILES['images[]']['error']= $files['error'][$key];
            $_FILES['images[]']['size']= $files['size'][$key];
            $fileName = $file_name .'_'. $image;
            $images[] = $fileName;
            $config['file_name'] = $fileName;
            $this->upload->initialize($config);
			if ($this->upload->do_upload('images[]')) {
				//$uploaded_file[]=$this->upload->data('file_name');
            } else {
                return false;
            }
        }

        return $images;
    }			
		
		
	function get_media_link($coloumn,$where="1,2,3,4,5,6,7,8")
	{
		return $id = $this -> db
					-> select('*')
					-> where_in($coloumn, $where)
					-> get('media_link')
					-> result_array();		
	}
		
		
	function row_delete($table,$column,$id)
	{
	   $this->db->where_in($column, explode(',',$id));
	   $this->db->delete($table); 
	}	
		
	function sqlQuery($table,$column,$where,$limit)
	{
		if($where){$this->db->where($column, $where);}
		if($limit){$this->db->limit($limit, 0);}
		$query = $this->db->get($table);
		return $query->result_array();
	}	
		
		
	function sqlPlainQuery($query)
	{
		$query = $this->db->query($query);
		return $query->result_array();
	}	
		
		
		
		
		
	function getLibArr($query,$column_id,$column_val)
	{
		$query = $this->db->query($query);
		$data_arr=array();
		foreach($query->result_array() as $rows){
			$data_arr[$rows[$column_id]]=$rows[$column_val];	
		}
		return $data_arr;
	}	
		
		
		
	public function showMessage($messageType){
			if($messageType==1){
				$msg='<h4 class="alert_warning">Success!This alert box could indicate a successful or positive action.</h4>';
  				
			}	
			else if($messageType==0){
				$msg='<h4 class="alert_warning">Error! You make some error.</h4>';
			}	
			else if($messageType==2){
				$msg='<h4 class="alert_warning">Success!</strong> This Data is Updated.
					</div>';
			}	
			else if($messageType==3){
				$msg='<h4 class="alert_warning">Success!</strong> This Data is Deleted.</h4>';
			}
			else if($messageType==4){
				$msg='<h4 class="alert_warning">Info!</strong> This alert box could indicate a neutral informative change or action.</h4>';
			}
			else if($messageType==5){
				$msg='<h4 class="alert_warning">Danger!</strong> This alert box could indicate a dangerous or potentially negative action.</h4>';
			}
			else if($messageType==6){
				$msg='<h4 class="alert_warning">Message hase been sent successfully.</h4>';
			}
			$this->session->set_flashdata('msg',$msg);	
		
		
		
		
		}

	
	
	

}
