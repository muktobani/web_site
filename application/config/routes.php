<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['login'] = 'admin/login';
$route['login/check_user'] = 'admin/login/check_user';
$route['dashboard'] = 'admin/dashboard';

$route['login'] = 'admin/login';
$route['login/log_out'] = 'admin/login/log_out';

//important link...............
$route['link'] 		  = 'admin/link';
$route['link/add'] 	  = 'admin/link/add';
$route['link/add/(:num)'] 	  = 'admin/link/add';
$route['link/save']   = 'admin/link/save';
$route['link/update'] = 'admin/link/update';
$route['link/delete/(:num)'] = 'admin/link/delete';


//directories...............
$route['directories'] 	   = 'admin/directories';
$route['directories/add']    = 'admin/directories/add';
$route['directories/add/(:num)']= 'admin/directories/add';
$route['directories/save']   = 'admin/directories/save';
$route['directories/update'] = 'admin/directories/update';
$route['directories/delete/(:num)'] = 'admin/directories/delete';



//members form...............
$route['members'] 	     = 'admin/members';
$route['members/add']    = 'admin/members/add';
$route['members/add/(:num)']= 'admin/members/add';
$route['members/save']   = 'admin/members/save';
$route['members/update'] = 'admin/members/update';
$route['members/delete/(:num)'] = 'admin/members/delete';
$route['members/approved'] = 'admin/members/approved';
$route['members/unapproved'] = 'admin/members/unapproved';
$route['members/send_mail'] = 'admin/members/send_mail';
$route['members/renew_approved'] = 'admin/members/renew_approved';



//album ...............
$route['album'] 	     = 'admin/album';
$route['album/add']    = 'admin/album/add';
$route['album/add/(:num)']= 'admin/album/add';
$route['album/save']   = 'admin/album/save';
$route['album/update'] = 'admin/album/update';
$route['album/delete/(:num)'] = 'admin/album/delete';

//Article ...............
$route['article'] 	     = 'admin/article';
$route['article/add']    = 'admin/article/add';
$route['article/add/(:num)']= 'admin/article/add';
$route['article/save']   = 'admin/article/save';
$route['article/update'] = 'admin/article/update';
$route['article/delete/(:num)'] = 'admin/article/delete';


//Country ...............
$route['country'] 	     = 'admin/country';
$route['country/add']    = 'admin/country/add';
$route['country/add/(:num)']= 'admin/country/add';
$route['country/save']   = 'admin/country/save';
$route['country/update'] = 'admin/country/update';
$route['country/delete/(:num)'] = 'admin/country/delete';



//Gallery ...............
$route['gallery'] 	     = 'admin/gallery';
$route['gallery/add']    = 'admin/gallery/add';
$route['gallery/add/(:num)']= 'admin/gallery/add';
$route['gallery/save']   = 'admin/gallery/save';
$route['gallery/update'] = 'admin/gallery/update';
$route['gallery/delete/(:num)'] = 'admin/gallery/delete';

//Ad Banner ...............
$route['ad_banner'] 	   = 'admin/ad_banner';
$route['ad_banner/add']    = 'admin/ad_banner/add';
$route['ad_banner/add/(:num)']= 'admin/ad_banner/add';
$route['ad_banner/save']   = 'admin/ad_banner/save';
$route['ad_banner/update'] = 'admin/ad_banner/update';
$route['ad_banner/delete/(:num)'] = 'admin/ad_banner/delete';


//admin_user ...............
$route['admin_user'] 	    = 'admin/admin_user';
$route['admin_user/add']    = 'admin/admin_user/add';
$route['admin_user/add/(:num)']= 'admin/admin_user/add';
$route['admin_user/save']   = 'admin/admin_user/save';
$route['admin_user/update'] = 'admin/admin_user/update';
$route['admin_user/delete/(:num)'] = 'admin/admin_user/delete';


//mail_template ...............
$route['mail_template'] 	   = 'admin/mail_template';
$route['mail_template/add']    = 'admin/mail_template/add';
$route['mail_template/add/(:num)']= 'admin/mail_template/add';
$route['mail_template/save']   = 'admin/mail_template/save';
$route['mail_template/update'] = 'admin/mail_template/update';
$route['mail_template/delete/(:num)'] = 'admin/mail_template/delete';
$route['mail_template/send/(:num)'] = 'admin/mail_template/send';

//mail ...............
$route['mail'] 	   	  = 'admin/mail';
$route['mail/add']    = 'admin/mail/add';
$route['mail/add/(:num)']= 'admin/mail/add';
$route['mail/send']   = 'admin/mail/send';
$route['mail/renew'] = 'admin/mail/update';
$route['mail/delete'] = 'admin/mail/delete';
$route['mail/read/(:num)'] = 'admin/mail/read';

//fees ...............
$route['fees'] 	   	  = 'admin/fees';
$route['fees/add']    = 'admin/fees/add';
$route['fees/add/(:num)']= 'admin/fees/add';
$route['fees/save']   = 'admin/fees/save';
$route['fees/update'] = 'admin/fees/update';
$route['fees/delete/(:num)'] = 'admin/fees/delete';



//event ...............
$route['event'] 	   = 'admin/event';
$route['event/add']    = 'admin/event/add';
$route['event/add/(:num)']= 'admin/event/add';
$route['event/save']   = 'admin/event/save';
$route['event/update'] = 'admin/event/update';
$route['event/delete/(:num)'] = 'admin/event/delete';


//-------------------------------member part-------------------------------------------------

//members reg ...............
$route['members_reg'] 	     = 'member/members_reg';
$route['members_reg/add']    = 'member/members_reg/add';
$route['members_reg/add/(:num)']= 'member/members_reg/add';
$route['members_reg/save']   = 'member/members_reg/save';
$route['members_reg/update'] = 'member/members_reg/update';
$route['members_reg/delete/(:num)'] = 'member/members_reg/delete';

//profile reg ...............
$route['profile'] 	     = 'member/profile';
$route['profile/add']    = 'member/profile/add';
$route['profile/add/(:num)']= 'member/profile/add';
$route['profile/save']   = 'member/profile/save';
$route['profile/update'] = 'member/profile/update';
$route['profile/change_password'] = 'member/profile/change_password';
$route['profile/change'] = 'member/profile/change_save';



//renew ...............
$route['renew'] 	   	  = 'member/renew';
$route['renew/add']    = 'member/renew/add';
$route['renew/add/(:num)']= 'member/renew/add';
$route['renew/save']   = 'member/renew/save';
$route['renew/renew'] = 'member/renew/update';
$route['renew/delete/(:num)'] = 'member/renew/delete';



//member_mail ...............
$route['member_mail'] 	   	  = 'member/member_mail';
$route['member_mail/add']    = 'member/member_mail/add';
$route['member_mail/add/(:num)']= 'member/member_mail/add';
$route['member_mail/send']   = 'member/member_mail/send';
$route['member_mail/renew'] = 'member/member_mail/update';
$route['member_mail/delete/(:num)'] = 'member/member_mail/delete';
$route['member_mail/read/(:num)'] = 'member/member_mail/read';


