<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ad_banner extends CI_Controller {

	function __construct() {
        parent::__construct();
		$this->load->helper(array('form'));
		$this->load->library('form_validation');
		$this->data['title']		= 'Ad banner';
		$this->data['description']	= 'description';
		$this->data['keywords']		= 'keywords';
		$this->data['current_page']	= 'Ad banner';
		$this->data['page_arr'] = $this->my_model->get_page();
		$this->data['position_arr'] = $this->my_model->get_ad_position();
		$this->data['status_arr'] = $this->my_model->get_status();
		$this->data['time_zone'] = date("Y-m-d h:i:s a");
    }
	
	public function index()
	{
		
		$this->data['container'] = 'admin/page/ad_banner_list_view';
		$this->load->view('admin/template',$this->data);
	}
	
	
	public function add()
	{
		$id=$this->uri->segment(3);
		if($id){$this->data['data_arr'] = $this->my_model->sqlQuery('ad_banner','ad_banner_id',$id,'');}
		$this->data['container'] = 'admin/page/ad_banner_add_view';
		$this->load->view('admin/template',$this->data);
	}
	
	public function save()
	{
		
		//$this->form_validation->set_rules('txt_ad_banner_photo[]', 'Photo', 'required');
		$this->form_validation->set_rules('txt_ad_banner_view_page', 'Link', 'required');
		$this->form_validation->set_rules('txt_ad_position', 'Ad Position', 'required');

		if ($this->form_validation->run() == FALSE)
		{
			$this->data['container'] = 'admin/page/ad_banner_add_view';
			$this->load->view('admin/template',$this->data);
		}
		else
		{
			
			if($_FILES['txt_ad_banner_photo']['name'][0]){
				$icon=$this->my_model->upload_files($_FILES['txt_ad_banner_photo'],time(), 'uploads/ad/');
				$icon=implode(',',$icon);
			}
			else
			{
				$icon='ad_banner.gif';
			}
			
			$this->ad_banner_id	= $this->my_model->get_max_id('ad_banner','ad_banner_id');
			$this->view_page   	= $this->input->post('txt_ad_banner_view_page');
			$this->ad_position  = $this->input->post('txt_ad_position');
			$this->banner_photo = $icon;
			$this->insert_time 	= $this->data['time_zone'];
			$this->insert_by 	= 1;
			$this->is_active 	= $this->input->post('cbo_status');
			$this->db->insert('ad_banner', $this);
			
			redirect(URL.'admin/ad_banner', 'refresh');
		}		
		
		
	}
	
	public function update()
	{
		$id=$this->input->post('ad_banner_id');
		//$this->form_validation->set_rules('txt_ad_banner_photo', 'Photo', 'required');
		$this->form_validation->set_rules('txt_ad_banner_view_page', 'Link', 'required');
		$this->form_validation->set_rules('txt_ad_position', 'Ad Position', 'required');

		if ($this->form_validation->run() == FALSE)
		{
			$this->data['container'] = 'admin/page/ad_banner_add_view';
			$this->load->view('admin/template',$this->data);
		}
		else
		{
			
			if($_FILES['txt_ad_banner_photo']['name'][0]){
				$icon=$this->my_model->upload_files($_FILES['txt_ad_banner_photo'],time(), 'uploads/ad/');
				$icon=implode(',',$icon);
				$this->banner_photo = $icon;
			}
			
			$this->view_page   	= $this->input->post('txt_ad_banner_view_page');
			$this->ad_position  = $this->input->post('txt_ad_position');
			$this->update_time = $this->data['time_zone'];
			$this->update_by = 1;
			$this->is_active = $this->input->post('cbo_status');
			
			$this->db->update('ad_banner', $this,array('ad_banner_id'=>$id));
			
			redirect(URL.'admin/ad_banner', 'refresh');
		}
	}
	
	
	public function delete()
	{
		$id=$this->uri->segment(3);
		$this->my_model->row_delete('ad_banner','ad_banner_id',$id);
		$this->data['container'] = 'admin/page/ad_banner_list_view';
		$this->load->view('admin/template',$this->data);
	}
	
	
	
}
