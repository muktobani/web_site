<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Country extends CI_Controller {

	function __construct() {
        parent::__construct();
		$this->load->helper(array('form'));
		$this->load->library('form_validation');
		$this->data['title']		= 'Country';
		$this->data['description']	= 'description';
		$this->data['keywords']		= 'keywords';
		$this->data['current_page']	= 'Country';
		$this->data['time_zone'] = date("Y-m-d h:i:s a");
		$this->data['status_arr'] = $this->my_model->get_status();
    }
	
	public function index()
	{
		
		$this->data['container'] = 'admin/page/country_list_view';
		$this->load->view('admin/template',$this->data);
	}
	
	
	public function add()
	{
		$id=$this->uri->segment(3);
		if($id){$this->data['data_arr'] = $this->my_model->sqlQuery('lib_country','country_id',$id,'');	}	
		$this->data['container'] = 'admin/page/country_add_view';
		$this->load->view('admin/template',$this->data);
	}
	
	public function save()
	{
		
		$this->form_validation->set_rules('txt_country_full_name', 'Country Full Name', 'required');

		if ($this->form_validation->run() == FALSE)
		{
			$this->data['container'] = 'admin/page/country_add_view';
			$this->load->view('admin/template',$this->data);
		}
		else
		{
			
			if($_FILES['txt_flag']['name'][0]){
				$icon=$this->my_model->upload_files($_FILES['txt_flag'],time(), 'uploads/flag/');
				$icon=implode(',',$icon);
			}
			else
			{
				$icon='country.gif';
			}
			
			$this->country_id= '';
			$this->country_full_name   	= $this->input->post('txt_country_full_name');
			$this->country_short_name   = $this->input->post('txt_country_short_name');
			$this->currency   = $this->input->post('txt_currency');
			$this->country_code   = $this->input->post('txt_country_code');
			$this->flag = $icon;
			$this->insert_time = $this->data['time_zone'];
			$this->insert_by = 1;
			$this->is_active = $this->input->post('cbo_status');
			$this->db->insert('lib_country', $this);
			
			$this->index();
		}		
		
		
	}
	
	public function update()
	{
		$id=$this->input->post('update_id');
		$this->form_validation->set_rules('txt_country_full_name', 'Country Full Name', 'required');

		if ($this->form_validation->run() == FALSE)
		{
			$this->data['container'] = 'admin/page/country_add_view';
			$this->load->view('admin/template',$this->data);
		}
		else
		{
			
			if($_FILES['txt_flag']['name'][0]){
				$icon=$this->my_model->upload_files($_FILES['txt_flag'],time(), 'uploads/flag/');
				$icon=implode(',',$icon);
				$this->flag = $icon;
			}
			
			$this->country_full_name  = $this->input->post('txt_country_full_name');
			$this->country_short_name = $this->input->post('txt_country_short_name');
			$this->country_code   = $this->input->post('txt_country_code');
			$this->currency   = $this->input->post('txt_currency');
			$this->update_time = $this->data['time_zone'];
			$this->update_by = 1;
			$this->is_active = $this->input->post('cbo_status');
			
			$this->db->update('lib_country', $this,array('country_id'=>$id));
			
			$this->data['container'] = 'admin/page/country_list_view';
			$this->load->view('admin/template',$this->data);
		}
	}
	
	
	public function delete()
	{
		$id=$this->uri->segment(3);
		$this->my_model->row_delete('lib_country','country_id',$id);
		$this->data['container'] = 'admin/page/country_list_view';
		$this->load->view('admin/template',$this->data);
	}
	
	
	
}
