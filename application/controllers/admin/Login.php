<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	function __construct() {
        parent::__construct();
		$this->load->helper(array('form'));
		$this->load->library('form_validation');
		$this->data['title']		= 'title';
		$this->data['description']	= 'description';
		$this->data['keywords']		= 'keywords';
		$this->data['current_page']	= 'login';
    }
	
	public function index()
	{
		$this->load->view('admin/page/login_view',$this->data);
	}
	
	
	public function check_user()
	{
		
		$username=$this->input->post('username');
		$password=$this->input->post('password');
		$login_type=$this->input->post('login_type');
		
		
		$this->form_validation->set_rules('login_type', 'Login Type', 'trim|required');
		$this->form_validation->set_rules('username', 'Username', 'trim|required');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');
		
		if($this->form_validation->run() == FALSE)
		{
		 $this->index();
		}
		else
		{

			if($login_type==1)
			{
				
			$data_arr = $this->my_model->sqlQuery('member_info','user_id',$username,1);
				if(count($data_arr)==1)
				{ 
					foreach($data_arr as $row)
					{ 
						if($row['password']==md5($password) && $row['user_id']==$username && $row['login_type']==1)
						{
							$newdata = array(
							'user_id'  => $row['member_id'],
							'username'  => $row['user_id'],
							'photo'  => $row['photo'],
							'email'  => $row['email'],
							'logged_in' => TRUE
							);
							
							$this->session->set_userdata($newdata);
							redirect(URL.'admin/dashboard');
							
						}
						else
						{
							redirect(URL.'admin/login');
						}
					}
					}
				else
				{
					redirect(URL.'admin/login');
				}
				
			
			}
			else
			{
				$data_arr = $this->my_model->sqlQuery('member_info','user_id',$username,1);
				
				if(count($data_arr)==1)
				{
					
					foreach($data_arr as $row)
					{ 
						if($row['password']==md5($password) && $row['user_id']==$username && $row['login_type']==2)
						{
							$newdata = array(
							'user_id'  => $row['member_id'],
							'username'  => $row['user_id'],
							'photo'  => $row['photo'],
							'email'  => $row['email'],
							'logged_in' => TRUE
							);
							
							$this->session->set_userdata($newdata);
							redirect(URL.'member/Profile');
							
						}
						else
						{
							redirect(URL.'admin/login');
						}
					}
				}
				else
				{
					redirect(URL.'admin/login');
				}
			
			}

		}	

	}
	
	
	public function log_out()
	{
		$this->session->sess_destroy();
		redirect('admin/login');
	}
	
	
	
	
}
