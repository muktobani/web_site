<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_user extends CI_Controller {

	function __construct() {
        parent::__construct();
		$this->load->helper(array('form'));
		$this->load->library('form_validation');
		$this->data['title']		= 'Admin User';
		$this->data['description']	= 'description';
		$this->data['keywords']		= 'keywords';
		$this->data['current_page']	= 'Admin User';
		$this->data['time_zone'] = date("Y-m-d h:i:s a");
		$this->data['status_arr'] = $this->my_model->get_status();
    }
	
	public function index()
	{
		
		$this->data['container'] = 'admin/page/admin_user_list_view';
		$this->load->view('admin/template',$this->data);
	}
	
	
	public function add()
	{
		$id=$this->uri->segment(3);
		if($id){$this->data['data_arr'] = $this->my_model->sqlQuery('member_info','member_id',$id,'');}
		$this->data['container'] = 'admin/page/admin_user_add_view';
		$this->load->view('admin/template',$this->data);
	}
	
	
	public function username_check($id)
	{
		$user_id=$this->my_model->sqlQuery('member_info','user_id',$id,'');
		if (!empty($user_id))
		{
			$this->form_validation->set_message('username_check', 'The {field} not available. Plz choose another {field}.');
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}
	
	public function save()
	{
		
		
		
		$this->form_validation->set_rules('txt_user_id', 'User Id', 'required|callback_username_check');
		$this->form_validation->set_rules('txt_user_name', 'User Name', 'required');
		$this->form_validation->set_rules('cbo_password', 'Password', 'required|matches[cbo_repassword]|min_length[4]|alpha_numeric');
		$this->form_validation->set_rules('cbo_repassword', 'Re password', 'required');

		if ($this->form_validation->run() == FALSE)
		{
			$this->data['container'] = 'admin/page/admin_user_add_view';
			$this->load->view('admin/template',$this->data);
		}
		else
		{
			
			if($_FILES['txt_photo']['name'][0]){
				$icon=$this->my_model->upload_files($_FILES['txt_photo'],time(), 'uploads/member/');
				$icon=implode(',',$icon);
			}
			else
			{
				$icon='default.gif';
			}
			
			$this->member_id= $this->my_model->get_max_id('member_info','member_id');
			$this->user_id  = $this->input->post('txt_user_id');
			$this->first_name= $this->input->post('txt_user_name');
			$this->password = md5($this->input->post('cbo_password'));
			$this->photo 	= $icon;
			$this->insert_time = $this->data['time_zone'];
			$this->insert_by = 1;
			$this->is_active = $this->input->post('cbo_status');
			$this->login_type = 1;
			$this->db->insert('member_info', $this);
			
			redirect(URL.'admin/admin_user');
		}		
		
		
	}
	
	public function update()
	{
		$id=$this->input->post('update_id');
		$this->form_validation->set_rules('txt_user_name', 'User Name', 'required');
		$this->form_validation->set_rules('cbo_password', 'Password', 'required|matches[cbo_repassword]|min_length[4]|alpha_numeric');
		$this->form_validation->set_rules('cbo_repassword', 'Re password', 'required');

		if ($this->form_validation->run() == FALSE)
		{
			
			if($id){$this->data['data_arr'] = $this->my_model->sqlQuery('member_info','member_id',$id,'');}
			$this->data['container'] = 'admin/page/admin_user_add_view';
			$this->load->view('admin/template',$this->data);
		}
		else
		{
		
			if($_FILES['txt_photo']['name'][0]){
				$icon=$this->my_model->upload_files($_FILES['txt_photo'],time(), 'uploads/member/');
				$icon=implode(',',$icon);
				$this->photo = $icon;
			}
			
			$this->first_name= $this->input->post('txt_user_name');
			$this->password = md5($this->input->post('cbo_password'));
			$this->update_time = $this->data['time_zone'];
			$this->update_by = 1;
			$this->is_active = $this->input->post('cbo_status');
			
			$this->db->update('member_info', $this,array('member_id'=>$id));
			
			$this->data['container'] = 'admin/page/admin_user_list_view';
			$this->load->view('admin/template',$this->data);
		}
	}
	
	
	public function delete()
	{
		$id=$this->uri->segment(3);
		$this->my_model->row_delete('member_info','member_id',$id);
		$this->data['container'] = 'admin/page/admin_user_list_view';
		$this->load->view('admin/template',$this->data);
	}
	
	
	
}
