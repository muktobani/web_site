<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Link extends CI_Controller {

	function __construct() {
        parent::__construct();
		$this->load->helper(array('form'));
		$this->load->library('form_validation');
		$this->data['title']		= 'Important Link';
		$this->data['description']	= 'description';
		$this->data['keywords']		= 'keywords';
		$this->data['current_page']	= 'link';
		$this->data['link_category'] = $this->my_model->get_link_category();
		$this->data['status_arr'] = $this->my_model->get_status();
		$this->data['time_zone'] = date("Y-m-d h:i:s a");
    }
	
	public function index()
	{
		
		$this->data['container'] = 'admin/page/link_list_view';
		$this->load->view('admin/template',$this->data);
	}
	
	
	public function add()
	{
		$id=$this->uri->segment(3);
		$this->data['data_arr'] = $this->my_model->get_media_link('media_link_id',$id);
		$this->data['container'] = 'admin/page/link_add_view';
		$this->load->view('admin/template',$this->data);
	}
	
	public function save()
	{
		
		$this->form_validation->set_rules('txt_title', 'Title', 'required');
		$this->form_validation->set_rules('txt_link', 'Link', 'required');

		if ($this->form_validation->run() == FALSE)
		{
			$this->data['container'] = 'admin/page/link_add_view';
			$this->load->view('admin/template',$this->data);
		}
		else
		{
			
			if($_FILES['txt_icon']['name'][0]){
				$icon=$this->my_model->upload_files($_FILES['txt_icon'],time(), 'uploads/icon/');
				$icon=implode(',',$icon);
			}
			else
			{
				$icon='default.gif';
			}
			
			$this->media_link_id= '';
			$this->title   	= $this->input->post('txt_title');
			$this->link    	= $this->input->post('txt_link');
			$this->category = $this->input->post('cbo_category');
			$this->link_icon = $icon;
			$this->insert_time = $this->data['time_zone'];
			$this->insert_by = 1;
			$this->is_active = $this->input->post('cbo_status');
			$this->db->insert('media_link', $this);
			
			$this->index();
		}		
		
		
	}
	
	public function update()
	{
		$id=$this->input->post('update_id');
		$this->form_validation->set_rules('txt_title', 'Title', 'required');
		$this->form_validation->set_rules('txt_link', 'Link', 'required');

		if ($this->form_validation->run() == FALSE)
		{
			$this->data['container'] = 'admin/page/link_add_view';
			$this->load->view('admin/template',$this->data);
		}
		else
		{
			
			if($_FILES['txt_icon']['name'][0]){
				$icon=$this->my_model->upload_files($_FILES['txt_icon'],time(), 'uploads/icon/');
				$icon=implode(',',$icon);
				$this->link_icon = $icon;
			}
			
			$this->title   	= $this->input->post('txt_title');
			$this->link    	= $this->input->post('txt_link');
			$this->category = $this->input->post('cbo_category');
			$this->update_time = $this->data['time_zone'];
			$this->update_by = 1;
			$this->is_active = $this->input->post('cbo_status');
			
			$this->db->update('media_link', $this,array('media_link_id'=>$id));
			
			$this->data['container'] = 'admin/page/link_list_view';
			$this->load->view('admin/template',$this->data);
		}
	}
	
	
	public function delete()
	{
		$id=$this->uri->segment(3);
		$this->my_model->row_delete('media_link','media_link_id',$id);
		$this->data['container'] = 'admin/page/link_list_view';
		$this->load->view('admin/template',$this->data);
	}
	
	
	
}
