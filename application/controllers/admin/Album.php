<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Album extends CI_Controller {

	function __construct() {
        parent::__construct();
		//$this->load->helper(array('form'));
		$this->load->library('form_validation');
		$this->data['title']		= 'Album';
		$this->data['description']	= 'description';
		$this->data['keywords']		= 'keywords';
		$this->data['current_page']	= 'Album';
		$this->data['time_zone'] 	= $this->my_model->get_timestamp();
		$this->data['status_arr'] = $this->my_model->get_status();
    }
	
	public function index()
	{
		
		$this->data['container'] = 'admin/page/album_list_view';
		$this->load->view('admin/template',$this->data);
	}
	
	
	public function add()
	{
		$id=$this->uri->segment(3);
		if($id){$this->data['data_arr'] = $this->my_model->sqlQuery('lib_album','album_id',$id,'');}
		$this->data['container'] = 'admin/page/album_add_view';
		$this->load->view('admin/template',$this->data);
	}
	
	public function save()
	{
		
		$this->form_validation->set_rules('txt_album_name', 'Album Name', 'required');

		if ($this->form_validation->run() == FALSE)
		{
			$this->data['container'] = 'admin/page/album_add_view';
			$this->load->view('admin/template',$this->data);
		}
		else
		{
			
			if($_FILES['txt_album_cover_photo']['name'][0]){
				$icon=$this->my_model->upload_files($_FILES['txt_album_cover_photo'],time(), 'uploads/album/');
				$icon=implode(',',$icon);
			}
			else
			{
				$icon='album.gif';
			}
			
			$this->album_id 		= $this->my_model->get_max_id('lib_album','album_id');
			$this->album_name   	= $this->input->post('txt_album_name');
			$this->description    	= $this->input->post('txt_description');
			$this->album_cover_photo= $icon;
			$this->insert_time 		= $this->data['time_zone'];
			$this->insert_by 		= 1;
			$this->is_active 		= $this->input->post('cbo_status');
			$this->db->insert('lib_album', $this);
			
			redirect(URL.'admin/album', 'refresh');
		}		
		
		
	}
	
	public function update()
	{
		$id=$this->input->post('update_id');
		$this->form_validation->set_rules('txt_album_name', 'Album Name', 'required');

		if ($this->form_validation->run() == FALSE)
		{
			$this->data['container'] = 'admin/page/album_add_view';
			$this->load->view('admin/template',$this->data);
		}
		else
		{
			
			if($_FILES['txt_album_cover_photo']['name'][0]){
				$icon=$this->my_model->upload_files($_FILES['txt_album_cover_photo'],time(), 'uploads/album/');
				$icon=implode(',',$icon);
				$this->album_cover_photo = $icon;
			}
			
			$this->album_name  	= $this->input->post('txt_album_name');
			$this->description 	= $this->input->post('txt_description');
			$this->update_time 	= $this->data['time_zone'];
			$this->update_by 	= 1;
			$this->is_active 	= $this->input->post('cbo_status');
			
			$this->db->update('lib_album', $this,array('album_id'=>$id));
			
			redirect(URL.'admin/album', 'refresh');

		}
	}
	
	
	public function delete()
	{
		$id=$this->uri->segment(3);
		$this->my_model->row_delete('lib_album','album_id',$id);
		$this->data['container'] = 'admin/page/album_list_view';
		$this->load->view('admin/template',$this->data);
	}
	
	
	
}
