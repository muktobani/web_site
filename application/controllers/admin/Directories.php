<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Directories extends CI_Controller {

	function __construct() {
        parent::__construct();
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		$this->data['title']		= 'Directories';
		$this->data['description']	= 'description';
		$this->data['keywords']		= 'keywords';
		$this->data['current_page']	= 'Directory';
		$this->data['time_zone'] 	= $this->my_model->get_timestamp();
		$this->data['status_arr']   = $this->my_model->get_status();
		$this->data['company_category'] = $this->my_model->get_company_category();
    }
	
	public function index()
	{
		$this->data['container'] = 'admin/page/directories_list_view';
		$this->load->view('admin/template',$this->data);
	}
	
	
	public function add()
	{
		$id=$this->uri->segment(3);
		if($id){$this->data['data_arr'] = $this->my_model->sqlQuery('directories','directories_id',$id,'');}
		$this->data['container'] = 'admin/page/directories_add_view';
		$this->load->view('admin/template',$this->data);
	}
	
	public function save()
	{ 
		
		$this->form_validation->set_rules('txt_company_name', 'Company Name', 'required');
		$this->form_validation->set_rules('txt_phone', 'phone', 'required');

		if ($this->form_validation->run() == FALSE)
		{
			$this->data['container'] = 'admin/page/directories_add_view';
			$this->load->view('admin/template',$this->data);
		}
		else
		{
			
			if($_FILES['txt_logo']['name'][0]){
				$icon=$this->my_model->upload_files($_FILES['txt_logo'],time(), 'uploads/directories/');
				$logo=implode(',',$icon);
			}
			else
			{
				$logo='article.gif';
			}
				
			if($_FILES['txt_gallery']['name'][0]){
				$icon=$this->my_model->upload_files($_FILES['txt_gallery'],time(), 'uploads/directories/');
				$gallery=implode(',',$icon);
			}
				
				$this->directories_id   = $this->my_model->get_max_id('directories','directories_id');
				$this->company_logo   	= $logo;
				$this->company_name   	= $this->input->post('txt_company_name');
				$this->web_address    	= $this->input->post('txt_web_address');
				$this->email_address 	= $this->input->post('txt_email_address');
				$this->phone 			= $this->input->post('txt_phone');
				$this->description 		= $this->input->post('txt_description');
				$this->google_map 		= $this->input->post('txt_google_map');
				$this->business_category= $this->input->post('cbo_business_category');
				$this->facebook 		= $this->input->post('txt_facebook');
				$this->google_plus 		= $this->input->post('txt_google_plus');
				$this->linkedin 		= $this->input->post('txt_linkedin');
				$this->twitter 			= $this->input->post('txt_twitter');
				$this->gallery   		= $gallery;

				$this->insert_time 		= $this->data['time_zone'];
				$this->insert_by 		= 1;
				$this->is_active 		= $this->input->post('cbo_status');
				$this->db->insert('directories', $this);
		
				redirect(URL.'admin/directories', 'refresh');
		}
	}
	
	



	public function update()
	{
		$id=$this->input->post('update_id');
		$this->form_validation->set_rules('txt_company_name', 'Company Name', 'required');
		$this->form_validation->set_rules('txt_phone', 'phone', 'required');

		if ($this->form_validation->run() == FALSE)
		{
			$this->data['container'] = 'admin/page/directories_add_view';
			$this->load->view('admin/template',$this->data);
		}
		else
		{
			
			if($_FILES['txt_logo']['name'][0]){
				$icon=$this->my_model->upload_files($_FILES['txt_logo'],time(), 'uploads/directories/');
				$logo=implode(',',$icon);
				$this->company_logo= $logo;
			}
				
			if($_FILES['txt_gallery']['name'][0]){
				$icon=$this->my_model->upload_files($_FILES['txt_gallery'],time(), 'uploads/directories/');
				$gallery=implode(',',$icon);
				$this->gallery= $gallery;
			}
			
			
			$this->company_name   	= $this->input->post('txt_company_name');
			$this->web_address    	= $this->input->post('txt_web_address');
			$this->email_address 	= $this->input->post('txt_email_address');
			$this->phone 			= $this->input->post('txt_phone');
			$this->description 		= $this->input->post('txt_description');
			$this->google_map 		= $this->input->post('txt_google_map');
			$this->business_category= $this->input->post('cbo_business_category');
			$this->facebook 		= $this->input->post('txt_facebook');
			$this->google_plus 		= $this->input->post('txt_google_plus');
			$this->linkedin 		= $this->input->post('txt_linkedin');
			$this->twitter 			= $this->input->post('txt_twitter');
			$this->update_time 	= $this->data['time_zone'];
			$this->update_by 	= 1;
			$this->is_active 	= $this->input->post('cbo_status');
			
			$this->db->update('directories', $this,array('directories_id'=>$id));
			
			redirect(URL.'admin/directories', 'refresh');

		}
	}
	
	
	public function delete()
	{
		$id=$this->uri->segment(3);
		$this->my_model->row_delete('directories','directories_id',$id);
		$this->data['container'] = 'admin/page/directories_list_view';
		$this->load->view('admin/template',$this->data);
	}
	













	
	
}
