<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Event extends CI_Controller {

	function __construct() {
        parent::__construct();
		$this->data['status_arr'] = $this->my_model->get_status();
		$this->data['time_zone'] = date("Y-m-d h:i:s a");
		$this->data['user_id'] =$this->session->userdata('user_id');

		$this->load->helper(array('form'));
		$this->load->library('form_validation');
		$this->load->helper('ckeditor');
		$this->data['title']		= 'Event';
		$this->data['description']	= 'description';
		$this->data['keywords']		= 'keywords';
		$this->data['current_page']	= 'Event';
		
		//Ckeditor's configuration
		$this->data['ckeditor'] = array(
		
			//ID of the textarea that will be replaced
			'id' 	=> 	'txt_event',
			'path'	=>	'js/ckeditor',
		
			//Optionnal values
			'config' => array(
				'toolbar' 	=> 	"Full", //Using the Full toolbar
				'width' 	=> 	"98%",	//Setting a custom width
				'height' 	=> 	'200px'	//Setting a custom height
					
			),
		
			//Replacing styles from the "Styles tool"
			'styles' => array(
			
				//Creating a new style named "style 1"
				'style 1' => array (
					'name' 		=> 	'Blue Title',
					'element' 	=> 	'h2',
					'styles' => array(
						'color' 	=> 	'Blue',
						'font-weight' 	=> 	'bold'
					)
				),
				
				//Creating a new style named "style 2"
				'style 2' => array (
					'name' 	  => 	'Red Title',
					'element' => 	'h2',
					'styles' => array(
						'color' 		 => 'Red',
						'font-weight' 	 => 'bold',
						'text-decoration'=> 'underline'
					)
				)				
			)
		);
    }
	
	
	public function index()
	{
		
		$this->data['container'] = 'admin/page/event_list_view';
		$this->load->view('admin/template',$this->data);
	}
	
	
	public function add()
	{
		$id=$this->uri->segment(3);
		if($id){$this->data['data_arr'] = $this->my_model->sqlQuery('event','event_id',$id,'');}
		$this->data['container'] = 'admin/page/event_add_view';
		$this->load->view('admin/template',$this->data);
	}
	
	public function save()
	{
		$this->form_validation->set_rules('cbo_category', 'Category', 'required');
		$this->form_validation->set_rules('txt_event', 'event', 'required');

		if ($this->form_validation->run() == FALSE)
		{
			$this->data['container'] = 'admin/page/event_add_view';
			$this->load->view('admin/template',$this->data);
		}
		else
		{
			
			if($_FILES['txt_event_photo']['name'][0]){
				$icon=$this->my_model->upload_files($_FILES['txt_event_photo'],time(), 'uploads/event/');
				$icon=implode(',',$icon);
			}
			else
			{
				$icon='event.gif';
			}
			
			$this->event_id	= $this->my_model->get_max_id('event','event_id');
			$this->category_id  = $this->input->post('cbo_category');
			$this->title    	= $this->input->post('txt_event_title');
			$this->photo		= $icon;
			$this->event	= $this->input->post('txt_event');
			$this->insert_time	= $this->data['time_zone'];
			$this->insert_by	= 1;
			$this->is_active 	= $this->input->post('cbo_status');
			
			$this->db->insert('event', $this);
			redirect(URL.'admin/event', 'refresh');
		}		
		
		
	}
	
	public function update()
	{
		$id=$this->input->post('update_id');
		$this->form_validation->set_rules('cbo_category', 'Category', 'required');
		$this->form_validation->set_rules('txt_event', 'event', 'required');

		if ($this->form_validation->run() == FALSE)
		{
			$this->data['container'] = 'admin/page/event_add_view';
			$this->load->view('admin/template',$this->data);
		}
		else
		{
			
			if($_FILES['txt_event_photo']['name'][0]){
				$icon=$this->my_model->upload_files($_FILES['txt_event_photo'],time(), 'uploads/event/');
				$icon=implode(',',$icon);
				$this->photo = $icon;
			}
			
			$this->category_id  = $this->input->post('cbo_category');
			$this->title    	= $this->input->post('txt_event_title');
			$this->event	= $this->input->post('txt_event');
			$this->update_by 	= 1;
			$this->is_active 	= $this->input->post('cbo_status');
			$this->db->update('event', $this,array('event_id'=>$id));
			
			redirect(URL.'admin/event', 'refresh');
		}
	}
	
	
	public function delete()
	{
		$id=$this->uri->segment(3);
		$this->my_model->row_delete('event','event_id',$id);
		$this->data['container'] = 'admin/page/event_list_view';
		$this->load->view('admin/template',$this->data);
	}
	
	
	
}
