<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mail_template extends CI_Controller {

	function __construct() {
        parent::__construct();
		$this->data['article_category'] = $this->my_model->get_article_category();
		$this->data['status_arr'] = $this->my_model->get_status();
		$this->data['time_zone'] = date("Y-m-d h:i:s a");
		$this->data['user_id'] =$this->session->userdata('user_id');

		$this->load->helper(array('form'));
		$this->load->library('form_validation');
		$this->load->helper('ckeditor');
		$this->data['title']		= 'Mail Template';
		$this->data['description']	= 'description';
		$this->data['keywords']		= 'keywords';
		$this->data['current_page']	= 'Mail Template';
		
		//Ckeditor's configuration
		$this->data['ckeditor'] = array(
		
			//ID of the textarea that will be replaced
			'id' 	=> 	'txt_mail_template',
			'path'	=>	'js/ckeditor',
		
			//Optionnal values
			'config' => array(
				'toolbar' 	=> 	"Full", //Using the Full toolbar
				'width' 	=> 	"98%",	//Setting a custom width
				'height' 	=> 	'140px'	//Setting a custom height
					
			),
		
			//Replacing styles from the "Styles tool"
			'styles' => array(
			
				//Creating a new style named "style 1"
				'style 1' => array (
					'name' 		=> 	'Blue Title',
					'element' 	=> 	'h2',
					'styles' => array(
						'color' 	=> 	'Blue',
						'font-weight' 	=> 	'bold'
					)
				),
				
				//Creating a new style named "style 2"
				'style 2' => array (
					'name' 	  => 	'Red Title',
					'element' => 	'h2',
					'styles' => array(
						'color' 		 => 'Red',
						'font-weight' 	 => 'bold',
						'text-decoration'=> 'underline'
					)
				)				
			)
		);
    }
	
	
	public function index()
	{
		
		$this->data['container'] = 'admin/page/mail_template_list_view';
		$this->load->view('admin/template',$this->data);
	}
	
	
	public function add()
	{
		$id=$this->uri->segment(3);
		if($id){$this->data['data_arr'] = $this->my_model->sqlQuery('mail_template','mail_template_id',$id,'');}
		$this->data['container'] = 'admin/page/mail_template_add_view';
		$this->load->view('admin/template',$this->data);
	}
	
	public function save()
	{
		$this->form_validation->set_rules('txt_mail_template_name', 'Mail Template Name', 'required');
		$this->form_validation->set_rules('txt_mail_subject', 'Mail Subject', 'required');
		$this->form_validation->set_rules('txt_mail_template', 'Mail Template', 'required');

		if ($this->form_validation->run() == FALSE)
		{
			$this->data['container'] = 'admin/page/mail_template_add_view';
			$this->load->view('admin/template',$this->data);
		}
		else
		{
			
			$this->mail_template_id	= $this->my_model->get_max_id('mail_template','mail_template_id');
			$this->mail_template_name= $this->input->post('txt_mail_template_name');
			$this->mail_subject    	= $this->input->post('txt_mail_subject');
			$this->mail_template    = $this->input->post('txt_mail_template');
			$this->insert_time	= $this->data['time_zone'];
			$this->insert_by	= 1;
			$this->is_active 	= $this->input->post('cbo_status');
			
			$this->db->insert('mail_template', $this);
			redirect(URL.'admin/mail_template', 'refresh');
		}		
		
		
	}
	
	public function update()
	{
		$id=$this->input->post('update_id');
		$this->form_validation->set_rules('txt_mail_template_name', 'Mail Template Name', 'required');
		$this->form_validation->set_rules('txt_mail_subject', 'Mail Subject', 'required');
		$this->form_validation->set_rules('txt_mail_template', 'Mail Template', 'required');

		if ($this->form_validation->run() == FALSE)
		{
			$this->data['container'] = 'admin/page/mail_template_add_view';
			$this->load->view('admin/template',$this->data);
		}
		else
		{
			
			
			$this->mail_template_name= $this->input->post('txt_mail_template_name');
			$this->mail_subject    	= $this->input->post('txt_mail_subject');
			$this->mail_template    = $this->input->post('txt_mail_template');
			$this->update_by 	= 1;
			$this->update_time	= $this->data['time_zone'];
			$this->is_active 	= $this->input->post('cbo_status');
			$this->db->update('mail_template', $this,array('mail_template_id'=>$id));
			
			redirect(URL.'admin/mail_template', 'refresh');
		}
	}
	
	
	public function send()
	{
		
		
		
		$email_arr = $this->my_model->getLibArr('select member_id,email from member_info','member_id','email');
		
		$email_arr=implode(',',$email_arr);
		$id=$this->uri->segment(3);
		//$this->my_model->row_delete('mail_template','mail_template_id',$id);
		$this->my_model->sendEmail('muktobani@gmail.com');
		
		$this->my_model->showMessage(6);
		
		$this->data['container'] = 'admin/page/mail_template_list_view';
		$this->load->view('admin/template',$this->data);
		
		
		
	}
	
	
	public function delete()
	{
		$id=$this->uri->segment(3);
		$this->my_model->row_delete('mail_template','mail_template_id',$id);
		$this->data['container'] = 'admin/page/mail_template_list_view';
		$this->load->view('admin/template',$this->data);
	}
	
	
	
	
	
}
