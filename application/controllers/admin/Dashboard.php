<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	function __construct() {
        parent::__construct();
        //$this->load->helper(form);
		$this->data['title']		= 'title';
		$this->data['description']	= 'description';
		$this->data['keywords']		= 'keywords';
		$this->data['current_page']	= 'Dashboard';
    }
	
	public function index()
	{
		$this->data['container']	= 'admin/page/dashboard_view';
		$this->load->view('admin/template',$this->data);
	}
}
