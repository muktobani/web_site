<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gallery extends CI_Controller {

	function __construct() {
        parent::__construct();
		//$this->load->helper(array('form'));
		$this->load->library('form_validation');
		$this->data['title']		= 'Gallery';
		$this->data['description']	= 'description';
		$this->data['keywords']		= 'keywords';
		$this->data['current_page']	= 'Gallery';
		$this->data['time_zone'] 	= date("Y-m-d h:i:s a");
		$this->data['album_arr'] = $this->my_model->getLibArr('select album_id,album_name from lib_album','album_id','album_name');
		$this->data['status_arr'] = $this->my_model->get_status();

    }
	
	public function index()
	{
		
		$this->data['container'] = 'admin/page/gallery_list_view';
		$this->load->view('admin/template',$this->data);
	}
	
	
	public function add()
	{
		$id=$this->uri->segment(3);
		if($id){$this->data['data_arr'] = $this->my_model->sqlQuery('lib_gallery','gallery_id',$id,'');}
		$this->data['container'] = 'admin/page/gallery_add_view';
		$this->load->view('admin/template',$this->data);
	}
	
	public function save()
	{
		
		$this->form_validation->set_rules('txt_title', 'Photo Title', 'required');

		if ($this->form_validation->run() == FALSE)
		{
			$this->data['container'] = 'admin/page/gallery_add_view';
			$this->load->view('admin/template',$this->data);
		}
		else
		{
			
			if($_FILES['txt_photo']['name'][0]){
				$icon=$this->my_model->upload_files($_FILES['txt_photo'],time(), 'uploads/gallery/');
				$icon=implode(',',$icon);
			}
			
			$this->gallery_id 		= $this->my_model->get_max_id('lib_gallery','gallery_id');
			$this->gallery_title   	= $this->input->post('txt_title');
			$this->album_id   	= $this->input->post('txt_album_id');
			$this->description    	= $this->input->post('txt_description');
			$this->photo= $icon;
			$this->insert_time 		= $this->data['time_zone'];
			$this->insert_by 		= 1;
			$this->is_active 		= $this->input->post('cbo_status');
			$this->db->insert('lib_gallery', $this);
			
			redirect(URL.'admin/gallery', 'refresh');
		}		
		
		
	}
	
	public function update()
	{
		$id=$this->input->post('update_id');
		$this->form_validation->set_rules('txt_title', 'gallery Name', 'required');

		if ($this->form_validation->run() == FALSE)
		{
			$this->data['container'] = 'admin/page/gallery_add_view';
			$this->load->view('admin/template',$this->data);
		}
		else
		{
			
			if($_FILES['txt_photo']['name'][0]){
				$icon=$this->my_model->upload_files($_FILES['txt_photo'],time(), 'uploads/gallery/');
				$icon=implode(',',$icon);
				$this->photo = $icon;
			}
			
			$this->gallery_title= $this->input->post('txt_title');
			$this->album_id   	= $this->input->post('txt_album_id');
			$this->description  = $this->input->post('txt_description');
			$this->update_time 	= $this->data['time_zone'];
			$this->update_by 	= 1;
			$this->is_active 	= $this->input->post('cbo_status');
			
			$this->db->update('lib_gallery', $this,array('gallery_id'=>$id));
			
			redirect(URL.'admin/gallery', 'refresh');
		}
	}
	
	
	public function delete()
	{
		$id=$this->uri->segment(3);
		$this->my_model->row_delete('lib_gallery','gallery_id',$id);
		$this->data['container'] = 'admin/page/gallery_list_view';
		$this->load->view('admin/template',$this->data);
	}
	
	
	
}
