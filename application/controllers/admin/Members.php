<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Members extends CI_Controller {

	function __construct() {
        parent::__construct();
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		$this->data['title']		= 'Members';
		$this->data['description']	= 'description';
		$this->data['keywords']		= 'keywords';
		$this->data['current_page']	= 'Members';
		$this->data['valid_year_fees_arr'] = $this->my_model->get_valid_year_fees();
		$this->data['yes_no_arr'] = $this->my_model->get_yes_no();
		$this->data['blood_group_arr'] = $this->my_model->get_blood_group();
		$this->data['name_title_arr'] = $this->my_model->get_name_titile();
		$this->data['member_type_arr'] = $this->my_model->get_member_type();
		$this->data['status_arr'] = $this->my_model->get_status();
		$this->data['time_zone'] = date("Y-m-d h:i:s a");
    }
	
	public function index()
	{
		
		$this->data['container'] = 'admin/page/members_list_view';
		$this->load->view('admin/template',$this->data);
	}
	
	
	public function add()
	{
		$id=$this->uri->segment(3);
		if($id){$this->data['data_arr'] = $this->my_model->sqlQuery('member_info','member_id',$id,'');}
		$this->data['countery_arr'] = $this->my_model->getLibArr('select country_id,country_full_name from lib_country','country_id','country_full_name');
		$this->data['container'] = 'admin/page/members_add_view';
		$this->load->view('admin/template',$this->data);
	}
	
	public function save()
	{
		
		
		$this->form_validation->set_rules('cbo_title', 'Title', 'trim|required');
		$this->form_validation->set_rules('cbo_first_name', 'First Name', 'trim|required');
		$this->form_validation->set_rules('txt_last_name', 'Last Name', 'trim|required');
		$this->form_validation->set_rules('txt_mobile', 'Mobile', 'trim|required');
		$this->form_validation->set_rules('txt_email', 'Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('txt_house_no', 'House No', 'trim|required');
		$this->form_validation->set_rules('txt_street_name', 'Street Name', 'trim|required');
		$this->form_validation->set_rules('txt_city', 'Suburb ', 'trim|required');
		$this->form_validation->set_rules('txt_ref_name', 'Refrred Full Name', 'trim|required');
		$this->form_validation->set_rules('cbo_blood_group', 'Glood Group', 'trim|required');
		$this->form_validation->set_rules('txt_is_blood_donate', 'Blood Donate?', 'trim|required');
		$this->form_validation->set_rules('txt_is_valunteer', 'Valunteer?', 'trim|required');
		$this->form_validation->set_rules('txt_ref_mobile', 'Refrred mobile', 'trim|required');
		$this->form_validation->set_rules('txt_member_type', 'Member Type', 'trim|required');
		$this->form_validation->set_rules('cbo_valid_year_fees', 'Year Fees', 'trim|required');
		$this->form_validation->set_rules('txt_is_agree', 'Agree', 'trim|required');
		
		//$this->form_validation->set_rules('txt_password', 'Password', 'trim|required|min_length[4]');
		//$this->form_validation->set_rules('txt_re_password', 'Password Confirmation', 'trim|required|matches[txt_password]');
		
		if ($this->form_validation->run() == FALSE)
		{
			$this->data['container'] = 'admin/page/members_add_view';
			$this->load->view('admin/template',$this->data);
		}
		else
		{
			
			if($_FILES['member_photo']['name'][0]){
				$photo=$this->my_model->upload_files($_FILES['member_photo'],time(), 'uploads/member/');
				$photo=implode(',',$photo);
			}
			else
			{
				$icon='member.gif';
			}
			
				$this->member_id = $this->my_model->get_max_id('member_info','member_id');
				$this->photo = $photo;
				$this->user_id = '';
				$this->password = md5('123456');
				$this->title = $this->input->post('cbo_title');
				
				$this->first_name = $this->input->post('cbo_first_name');
				$this->middle_name = $this->input->post('txt_middle_name');
				$this->last_name = $this->input->post('txt_last_name');
				
				$this->nick_name = $this->input->post('txt_nick_name');
				$this->birth_date = $this->input->post('txt_birth_date');
				$this->phone = $this->input->post('txt_phone');
				$this->mobile = $this->input->post('txt_mobile');
				$this->email = $this->input->post('txt_email');
				$this->house_no = $this->input->post('txt_house_no');
				$this->street_name = $this->input->post('txt_street_name');
				$this->city = $this->input->post('txt_city');
				$this->zip = $this->input->post('txt_zip');
				$this->ref_name = $this->input->post('txt_ref_name');
				$this->ref_mobile = $this->input->post('txt_ref_mobile');
				$this->blood_group = $this->input->post('cbo_blood_group');
				$this->is_blood_donate = $this->input->post('txt_is_blood_donate');
				$this->is_valunteer = $this->input->post('txt_is_valunteer');
				$this->member_type = $this->input->post('txt_member_type');
				$this->valid_year = $this->input->post('cbo_valid_year_fees');
				$this->is_agree = $this->input->post('txt_is_agree');
				
				$this->insert_time = $this->data['time_zone'];
				$this->insert_by = 1;
				$this->is_active = $this->input->post('cbo_status');
				
				$this->db->insert('member_info', $this);
				redirect(URL.'admin/members', 'refresh');
		}
		
	}
	
	public function update()
	{
		$id=$this->input->post('update_id');
		$this->form_validation->set_rules('cbo_title', 'Title', 'trim|required');
		$this->form_validation->set_rules('cbo_first_name', 'First Name', 'trim|required');
		$this->form_validation->set_rules('txt_last_name', 'Last Name', 'trim|required');
		$this->form_validation->set_rules('txt_mobile', 'Mobile', 'trim|required');
		$this->form_validation->set_rules('txt_email', 'Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('txt_house_no', 'House No', 'trim|required');
		$this->form_validation->set_rules('txt_street_name', 'Street Name', 'trim|required');
		$this->form_validation->set_rules('txt_city', 'Suburb ', 'trim|required');
		$this->form_validation->set_rules('txt_ref_name', 'Refrred Full Name', 'trim|required');
		$this->form_validation->set_rules('txt_ref_mobile', 'Refrred mobile', 'trim|required');
		$this->form_validation->set_rules('cbo_blood_group', 'Glood Group', 'trim|required');
		$this->form_validation->set_rules('txt_is_blood_donate', 'Blood Donate?', 'trim|required');
		$this->form_validation->set_rules('txt_is_valunteer', 'Valunteer?', 'trim|required');
		$this->form_validation->set_rules('txt_member_type', 'Member Type', 'trim|required');
		$this->form_validation->set_rules('cbo_valid_year_fees', 'Year Fees', 'trim|required');
		$this->form_validation->set_rules('txt_is_agree', 'Agree', 'trim|required');

		if ($this->form_validation->run() == FALSE)
		{
			$this->data['container'] = 'admin/page/members_add_view';
			$this->load->view('admin/template',$this->data);
		}
		else
		{
			
			if($_FILES['member_photo']['name'][0]){
				$photo=$this->my_model->upload_files($_FILES['member_photo'],time(), 'uploads/member/');
				$photo=implode(',',$photo);
				$this->photo = $photo;
			}
			

				$this->title = $this->input->post('cbo_title');
				$this->first_name = $this->input->post('cbo_first_name');
				$this->middle_name = $this->input->post('txt_middle_name');
				$this->last_name = $this->input->post('txt_last_name');
				$this->nick_name = $this->input->post('txt_nick_name');
				$this->birth_date = $this->input->post('txt_birth_date');
				$this->phone = $this->input->post('txt_phone');
				$this->mobile = $this->input->post('txt_mobile');
				$this->email = $this->input->post('txt_email');
				$this->house_no = $this->input->post('txt_house_no');
				$this->street_name = $this->input->post('txt_street_name');
				$this->city = $this->input->post('txt_city');
				$this->zip = $this->input->post('txt_zip');
				$this->ref_name = $this->input->post('txt_ref_name');
				$this->ref_mobile = $this->input->post('txt_ref_mobile');
				$this->blood_group = $this->input->post('cbo_blood_group');
				$this->is_blood_donate = $this->input->post('txt_is_blood_donate');
				$this->is_valunteer = $this->input->post('txt_is_valunteer');
				$this->member_type = $this->input->post('txt_member_type');
				$this->valid_year = $this->input->post('cbo_valid_year_fees');
				$this->is_agree = $this->input->post('txt_is_agree');
				$this->update_time 	= $this->data['time_zone'];
				$this->update_by 	= 1;
				$this->is_active 	= $this->input->post('cbo_status');
			
			$this->db->update('member_info', $this,array('member_id'=>$id));
			
			redirect(URL.'admin/members', 'refresh');

		}
	}
	
	
	public function delete()
	{
		$id=$this->uri->segment(3);
		$this->my_model->row_delete('member_info','member_id',$id);
		$this->data['container'] = 'admin/page/members_list_view';
		$this->load->view('admin/template',$this->data);
	}
	
	
	
	
	public function approved()
	{
		$member_id=$this->input->post('data');
		$this->is_active = 1;
		if($this->db->update('member_info', $this,array('member_id'=>$member_id))){
			echo 'Approved Success';
		}
		else
		{
			echo 'Not Approved';
		}
		
	}
	
	
	public function unapproved()
	{
		$member_id=$this->input->post('data');
		$this->is_active 	= 0;
		$this->expair_date 	= date("Y-m-d h:t:s a",time());
		if($this->db->update('member_info', $this,array('member_id'=>$member_id))){
			echo 'Unapproved Success';
		}
		else
		{
			echo 'Not Unapproved';
		}
	}
	
	public function renew_approved()
	{
		$member_id=$this->input->post('data');
		if($member_id){$this->data['data_arr'] = $this->my_model->sqlQuery('member_info','member_id',$member_id,'');}
		
		$expair_date=date("Y-m-d h:t:s a",strtotime($this->data['data_arr'][0]['renew_apply_for'].' year', strtotime($this->data['data_arr'][0]['expair_date'])));	
		
		$this->expair_date = $expair_date;
		if($this->db->update('member_info', $this,array('member_id'=>$member_id))){
			echo 'Renew approved Success';
		}
		else
		{
			echo 'Unable to Renew';
		}
		exit();
	}
	
	public function send_mail()
	{
		$member_id=$this->input->post('data');
		$email_arr = $this->my_model->getLibArr("select member_id,email from member_info where member_id in($member_id)",'member_id','email');
		$mail_list = implode(',',$email_arr);
		
		if($this->my_model->sendEmail($mail_list)){
			echo 'Mail Senld Success';
		}
		else
		{
			echo 'Mail Not Send';
		}
		
		
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
}
