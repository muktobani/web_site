<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Member_mail extends CI_Controller {

	function __construct() {
        parent::__construct();
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		$this->data['title']		= 'member_mail';
		$this->data['description']	= 'description';
		$this->data['keywords']		= 'keywords';
		$this->data['current_page']	= 'member_mail';
		$this->data['time_zone'] = date("Y-m-d h:i:s a");
		$this->data['user_id'] =$this->session->userdata('user_id');
		$this->data['email'] =$this->session->userdata('email');
		$this->data['status_arr'] = $this->my_model->get_status();
		$this->data['username'] = $this->session->userdata('username');
    }
	
	public function index()
	{ 	//$user_id =$this->session->userdata('user_id');
		//if($user_id){$this->data['data_arr'] = $this->my_model->sqlQuery('mail','send_by',$user_id,'');}
		$this->data['container'] = 'member/page/member_mail_list_view';
		$this->load->view('member/template',$this->data);
	}
	
	
	public function add()
	{
		$id=$this->uri->segment(3);
		if($id){$this->data['data_arr'] = $this->my_model->sqlQuery('mail','mail_id',$id,'');}
		$this->data['container'] = 'member/page/member_mail_add_view';
		$this->load->view('member/template',$this->data);
	}
	
	public function read()
	{
		$id=$this->uri->segment(3);
		if($id){$this->data['data_arr'] = $this->my_model->sqlQuery('mail','mail_id',$id,'');}
		$this->data['container'] = 'member/page/member_mail_read_view';
		$this->load->view('member/template',$this->data);
	
		$this->is_read = 1;
		$this->db->update('mail', $this,array('mail_id'=>$id,'send_to'=>$this->data['email']));


	
	}
	
	public function send()
	{
		$this->form_validation->set_rules('txt_subject', 'Subject', 'required');
		$this->form_validation->set_rules('txt_message', 'Message', 'required');

		if ($this->form_validation->run() == FALSE)
		{
			$this->data['container'] = 'member/page/member_mail_add_view';
			$this->load->view('member/template',$this->data);
		}
		else
		{
			
			if($_FILES['txt_attach_file']['name'][0]){
				$icon=$this->my_model->upload_files($_FILES['txt_attach_file'],time(), 'uploads/mail/');
				$attach_file=implode(',',$icon);
				$this->attach_file	= $attach_file;
			}
			
			$this->mail_id	= $this->my_model->get_max_id('mail','mail_id');
			$this->send_to  = $this->input->post('txt_send_to');
			$this->mail_subject  = $this->input->post('txt_subject');
			$this->mail_body  = $this->input->post('txt_message');
			$this->is_read	= 0;
			$this->send_by	= $this->data['email'];

			$this->insert_time	= $this->data['time_zone'];
			$this->insert_by	= $this->data['user_id'];
			$this->is_active 	= $this->input->post('cbo_status');
			
			$this->db->insert('mail', $this);
			
			$this->my_model->showMessage(6);
			redirect(URL.'member/member_mail');
		}		
		
		
	}
	
		
	
	
	public function oldpassword_check($old_password)
	{
	   $old_password_hash = md5($old_password);
		
		$id=$this->session->userdata('user_id');
		if($id){$this->data['old_password_arr'] = $this->my_model->sqlQuery('member_mail_info','member_mail_id',$id,'');}
		$old_password_db_hash=$this->data['old_password_arr'][0][password];
	
	   if($old_password_hash != $old_password_db_hash)
	   {
		  //$this->form_validation->set_message('txt_password', 'Old password not match');
		  return FALSE;
	   }
	   else
	   { 
	   	 return TRUE;
	   }
	}	
		
	
	
	
	
}
