<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {

	function __construct() {
        parent::__construct();
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		$this->data['title']		= 'Members';
		$this->data['description']	= 'description';
		$this->data['keywords']		= 'keywords';
		$this->data['current_page']	= 'Members';
		$this->data['valid_year_arr'] = $this->my_model->get_renew_year();
		$this->data['valid_year_fees_arr'] = $this->my_model->get_valid_year_fees();
		$this->data['yes_no_arr'] = $this->my_model->get_yes_no();
		$this->data['blood_group_arr'] = $this->my_model->get_blood_group();
		$this->data['name_title_arr'] = $this->my_model->get_name_titile();
		$this->data['member_type_arr'] = $this->my_model->get_member_type();
		$this->data['time_zone'] = date("Y-m-d h:i:s a");
		
    }
	
	public function index()
	{ 	$user_id =$this->session->userdata('user_id');
		if($user_id){$this->data['data_arr'] = $this->my_model->sqlQuery('member_info','member_id',$user_id,'');}
		$this->data['container'] = 'member/page/profile_view';
		$this->load->view('member/template',$this->data);
	}
	
	
	public function add()
	{
		$id=$this->session->userdata('user_id');
		if($id){$this->data['data_arr'] = $this->my_model->sqlQuery('member_info','member_id',$id,'');}
		$this->data['container'] = 'member/page/profile_add_view';
		$this->load->view('member/template',$this->data);
	}
	
	
	public function update()
	{
		$id=$this->input->post('update_id');
		$this->form_validation->set_rules('cbo_title', 'Title', 'trim|required');
		$this->form_validation->set_rules('cbo_first_name', 'First Name', 'trim|required');
		$this->form_validation->set_rules('txt_last_name', 'Last Name', 'trim|required');
		$this->form_validation->set_rules('txt_mobile', 'Mobile', 'trim|required');
		$this->form_validation->set_rules('txt_email', 'Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('txt_house_no', 'House No', 'trim|required');
		$this->form_validation->set_rules('txt_street_name', 'Street Name', 'trim|required');
		$this->form_validation->set_rules('txt_city', 'Suburb ', 'trim|required');
		$this->form_validation->set_rules('txt_ref_name', 'Refrred Full Name', 'trim|required');
		$this->form_validation->set_rules('txt_ref_mobile', 'Refrred mobile', 'trim|required');
		$this->form_validation->set_rules('cbo_blood_group', 'Glood Group', 'trim|required');
		$this->form_validation->set_rules('txt_is_blood_donate', 'Blood Donate?', 'trim|required');
		$this->form_validation->set_rules('txt_is_valunteer', 'Valunteer?', 'trim|required');
		$this->form_validation->set_rules('txt_member_type', 'Member Type', 'trim|required');
		$this->form_validation->set_rules('cbo_valid_year_fees', 'Year Fees', 'trim|required');

		if ($this->form_validation->run() == FALSE)
		{
			$this->data['container'] = 'member/page/profile_view';
			$this->load->view('member/template',$this->data);
		}
		else
		{
			
			if($_FILES['member_photo']['name'][0]){
				$photo=$this->my_model->upload_files($_FILES['member_photo'],time(), 'uploads/member/');
				$photo=implode(',',$photo);
				$this->photo = $photo;
			}
			

				$this->title = $this->input->post('cbo_title');
				$this->first_name = $this->input->post('cbo_first_name');
				$this->middle_name = $this->input->post('txt_middle_name');
				$this->last_name = $this->input->post('txt_last_name');
				$this->nick_name = $this->input->post('txt_nick_name');
				$this->birth_date = $this->input->post('txt_birth_date');
				$this->phone = $this->input->post('txt_phone');
				$this->mobile = $this->input->post('txt_mobile');
				$this->email = $this->input->post('txt_email');
				$this->house_no = $this->input->post('txt_house_no');
				$this->street_name = $this->input->post('txt_street_name');
				$this->city = $this->input->post('txt_city');
				$this->zip = $this->input->post('txt_zip');
				$this->ref_name = $this->input->post('txt_ref_name');
				$this->ref_mobile = $this->input->post('txt_ref_mobile');
				$this->blood_group = $this->input->post('cbo_blood_group');
				$this->is_blood_donate = $this->input->post('txt_is_blood_donate');
				$this->is_valunteer = $this->input->post('txt_is_valunteer');
				$this->member_type = $this->input->post('txt_member_type');
				$this->valid_year = $this->input->post('cbo_valid_year_fees');
				$this->update_time 	= $this->data['time_zone'];
				$this->update_by 	= 1;
			
			$this->db->update('member_info', $this,array('member_id'=>$id));
			
			redirect(URL.'member/profile', 'refresh');

		}
	}
	
	
	public function change_password()
	{
		$id=$this->session->userdata('user_id');
		if($id){$this->data['data_arr'] = $this->my_model->sqlQuery('member_info','member_id',$id,'');}
		$this->data['container'] = 'member/page/change_password_add_view';
		$this->load->view('member/template',$this->data);
	}
	
	
	public function change_save()
	{
		
		$id=$this->session->userdata('user_id');
		if($id){$this->data['data_arr'] = $this->my_model->sqlQuery('member_info','member_id',$id,'');}
		
		$id=$this->input->post('update_id');
		$this->form_validation->set_rules('txt_password', 'Old Password Not Match', 'trim|required|callback_oldpassword_check');
		$this->form_validation->set_rules('txt_new_password', 'New Passowrd', 'trim|required');
		$this->form_validation->set_rules('txt_conf_password', 'Confirm password not match', 'trim|required|matches[txt_new_password]');

		if ($this->form_validation->run() == FALSE)
		{
			$this->data['container'] = 'member/page/change_password_add_view';
			$this->load->view('member/template',$this->data);
		}
		else
		{
			
			$this->password = md5($this->input->post('txt_new_password'));
			$this->db->update('member_info', $this,array('member_id'=>$id));
			redirect(URL.'member/profile', 'refresh');

		}
	}
	
	
	
	
	public function oldpassword_check($old_password)
	{
	   $old_password_hash = md5($old_password);
		
		$id=$this->session->userdata('user_id');
		if($id){$this->data['old_password_arr'] = $this->my_model->sqlQuery('member_info','member_id',$id,'');}
		$old_password_db_hash=$this->data['old_password_arr'][0][password];
	
	   if($old_password_hash != $old_password_db_hash)
	   {
		  $this->form_validation->set_message('txt_password', 'Old password not match');
		  return FALSE;
	   } 
	   return TRUE;
	}	
		
	
	
}
