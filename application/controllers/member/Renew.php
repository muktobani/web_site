<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Renew extends CI_Controller {

	function __construct() {
        parent::__construct();
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		$this->data['title']		= 'Members';
		$this->data['description']	= 'description';
		$this->data['keywords']		= 'keywords';
		$this->data['current_page']	= 'Members';
		$this->data['valid_year_arr'] = $this->my_model->get_renew_year();
		$this->data['valid_year_fees_arr'] = $this->my_model->get_valid_year_fees();
		$this->data['yes_no_arr'] = $this->my_model->get_yes_no();
		$this->data['blood_group_arr'] = $this->my_model->get_blood_group();
		$this->data['name_title_arr'] = $this->my_model->get_name_titile();
		$this->data['member_type_arr'] = $this->my_model->get_member_type();
		$this->data['time_zone'] = date("Y-m-d h:i:s a");
		
    }
	
	public function index()
	{ 	$user_id =$this->session->userdata('user_id');
		if($user_id){$this->data['data_arr'] = $this->my_model->sqlQuery('member_info','member_id',$user_id,'');}
		$this->data['container'] = 'member/page/renew_add_view';
		$this->load->view('member/template',$this->data);
	}
	
	
	public function add()
	{
		$id=$this->session->userdata('user_id');
		if($id){$this->data['data_arr'] = $this->my_model->sqlQuery('member_info','member_id',$id,'');}
		$this->data['container'] = 'member/page/renew_add_view';
		$this->load->view('member/template',$this->data);
	}
	
	
	public function update()
	{
		
		$id=$this->session->userdata('user_id');
		if($id){$this->data['data_arr'] = $this->my_model->sqlQuery('member_info','member_id',$id,'');}
		 
		$id=$this->input->post('update_id');
		$this->form_validation->set_rules('txt_password', 'Current Password Not Match', 'trim|required|callback_oldpassword_check');
		$this->form_validation->set_rules('cbo_renew_apply_for', 'Renew Year & Fees', 'trim|required');

		if ($this->form_validation->run() == FALSE)
		{
			$this->data['container'] = 'member/page/renew_add_view';
			$this->load->view('member/template',$this->data);
		}
		else
		{
			
			$password = md5($this->input->post('txt_password'));
			$this->renew_apply_for = $this->input->post('cbo_renew_apply_for');
			$this->db->update('member_info', $this,array('member_id'=>$id,'password'=>$password));
			redirect(URL.'member/profile', 'refresh');

		}
	}
	
	
	
	
	
	public function oldpassword_check($old_password)
	{
	   $old_password_hash = md5($old_password);
		
		$id=$this->session->userdata('user_id');
		if($id){$this->data['old_password_arr'] = $this->my_model->sqlQuery('member_info','member_id',$id,'');}
		$old_password_db_hash=$this->data['old_password_arr'][0][password];
	
	   if($old_password_hash != $old_password_db_hash)
	   {
		  //$this->form_validation->set_message('txt_password', 'Old password not match');
		  return FALSE;
	   }
	   else
	   { 
	   	 return TRUE;
	   }
	}	
		
	
	
	
	
}
