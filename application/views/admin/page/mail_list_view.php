<h4 class="button_bar">
    <a href="<?php echo URL;?>mail/add" class="href_btn">New Mail</a>
    <a href="javascript:submit_page('<?php echo URL;?>mail/','delete')" class="href_btn">Delete</a>
</h4>

<?php
$member_email=$this->session->userdata('email');
$inbox_arr=$this->my_model->sqlQuery('mail','send_to',$member_email,'');
$read_arr=$this->my_model->sqlPlainQuery("select * from mail where send_to = '$member_email' and is_read=1");
$unread_arr=$this->my_model->sqlPlainQuery("select * from mail where send_to = '$member_email' and is_read=0");

$send_arr=$this->my_model->sqlPlainQuery("select * from mail where send_by = '$member_email' and is_active=1");
$draft_arr=$this->my_model->sqlPlainQuery("select * from mail where send_by = '$member_email' and is_active=0");


?>		
		<article class="module width_4_quarter">
		<header><h3 class="tabs_involved">Mail List</h3>
		<ul class="tabs">
   			<li><a href="#tab1">Inbox(<?php echo count($inbox_arr);?>)</a></li>
    		<li><a href="#tab2">Read(<?php echo count($read_arr);?>)</a></li>
    		<li><a href="#tab3">Unread(<?php echo count($unread_arr);?>)</a></li>
    		<li><a href="#tab4">Send Mail(<?php echo count($send_arr);?>)</a></li>
   			<li><a href="#tab5">Draft(<?php echo count($draft_arr);?>)</a></li>
		</ul>
		</header>

		<div class="tab_container">
			<div id="tab1" class="tab_content">
			<table class="tablesorter" cellspacing="0"> 
			<thead> 
				<tr> 
   					<td width="15" title="Select/Unselect All"><input type="checkbox" id="selecctall" ></td> 
    				<th>Subject</th> 
    				<th>Date & Time</th> 
    				<th>Attach File</th> 
				</tr> 
			</thead> 
			<tbody> 
				<?php foreach($inbox_arr as $rows){?>
                <tr> 
   					<td align="right"><input type="checkbox" class="select" name="select" value="<?php echo $rows['mail_id'];?>"></td> 
    				<td><a href="<?php echo URL.'mail/read/'.$rows[mail_id];?>"><?php echo $rows['mail_subject'];?></a></td> 
    				<td><?php echo $rows['insert_time'];?></td> 
                    <td width="60" align="center">
                    <a href="<?php echo URL.'uploads/mail/'.$rows['attach_file'];?>" target="_new"><input type="image" src="<?php echo URL;?>images/download.png" title="Download"></a>
                   
                    </td> 
				</tr> 
                <?php } ?>
			</tbody> 
			</table>
			</div><!-- end of #tab1 -->
			
			<div id="tab2" class="tab_content">
			<table class="tablesorter" cellspacing="0"> 
			<thead> 
				<tr> 
   					<td width="15" title="Select/Unselect All"><input type="checkbox" id="selecctall2" ></td> 
    				<th>Subject</th> 
    				<th>Date & Time</th> 
    				<th>Attach File</th> 
				</tr> 
			</thead> 
			<tbody> 
				<?php foreach($read_arr as $rows){?>
                <tr> 
   					<td align="right"><input type="checkbox" class="select2" name="select" value="<?php echo $rows['mail_id'];?>"></td> 
    				<td><a href="<?php echo URL.'mail/read/'.$rows[mail_id];?>"><?php echo $rows['mail_subject'];?></a></td> 
    				<td><?php echo $rows['insert_time'];?></td> 
                    <td width="60" align="center">
                    <a href="<?php echo URL.'uploads/mail/'.$rows['attach_file'];?>" target="_new"><input type="image" src="<?php echo URL;?>images/download.png" title="Download"></a>
                   
                    </td> 
				</tr> 
                <?php } ?>
			</tbody> 
			</table>

			</div><!-- end of #tab2 -->
			
            
			<div id="tab3" class="tab_content">
			<table class="tablesorter" cellspacing="0"> 
			<thead> 
				<tr> 
   					<td width="15" title="Select/Unselect All"><input type="checkbox" id="selecctall3" ></td> 
    				<th>Subject</th> 
    				<th>Date & Time</th> 
    				<th>Attach File</th> 
				</tr> 
			</thead> 
			<tbody> 
				<?php foreach($unread_arr as $rows){?>
                <tr> 
   					<td align="right"><input type="checkbox" class="select3" name="select" value="<?php echo $rows['mail_id'];?>"></td> 
    				<td><a href="<?php echo URL.'mail/read/'.$rows[mail_id];?>"><?php echo $rows['mail_subject'];?></a></td> 
    				<td><?php echo $rows['insert_time'];?></td> 
                    <td width="60" align="center">
                    <a href="<?php echo URL.'uploads/mail/'.$rows['attach_file'];?>" target="_new"><input type="image" src="<?php echo URL;?>images/download.png" title="Download"></a>
                   
                    </td> 
				</tr> 
                <?php } ?>
			</tbody> 
			</table>
			</div>            
			
            <div id="tab4" class="tab_content">
			<table class="tablesorter" cellspacing="0"> 
			<thead> 
				<tr> 
   					<td width="15" title="Select/Unselect All"><input type="checkbox" id="selecctall4" ></td> 
    				<th>Subject</th> 
    				<th>Date & Time</th> 
    				<th>Attach File</th> 
				</tr> 
			</thead> 
			<tbody> 
				<?php foreach($send_arr as $rows){?>
                <tr> 
   					<td align="right"><input type="checkbox" class="select4" name="select" value="<?php echo $rows['mail_id'];?>"></td> 
    				<td><a href="<?php echo URL.'mail/read/'.$rows[mail_id];?>"><?php echo $rows['mail_subject'];?></a></td> 
    				<td><?php echo $rows['insert_time'];?></td> 
                    <td width="60" align="center">
                    <a href="<?php echo URL.'uploads/mail/'.$rows['attach_file'];?>" target="_new"><input type="image" src="<?php echo URL;?>images/download.png" title="Download"></a>
				</tr> 
                <?php } ?>
			</tbody> 
			</table>
			</div>            
			
            <div id="tab5" class="tab_content">
			<table class="tablesorter" cellspacing="0"> 
			<thead> 
				<tr> 
   					<td width="15" title="Select/Unselect All"><input type="checkbox" id="selecctall5" ></td> 
    				<th>Subject</th> 
    				<th>Date & Time</th> 
    				<th>Attach File</th> 
				</tr> 
			</thead> 
			<tbody> 
				<?php foreach($draft_arr as $rows){?>
                <tr> 
   					<td align="right"><input type="checkbox" class="select5" name="select" value="<?php echo $rows['mail_id'];?>"></td> 
    				<td><a href="<?php echo URL.'mail/read/'.$rows[mail_id];?>"><?php echo $rows['mail_subject'];?></a></td> 
    				<td><?php echo $rows['insert_time'];?></td> 
                    <td width="60" align="center">
                    <a href="<?php echo URL.'uploads/mail/'.$rows['attach_file'];?>" target="_new"><input type="image" src="<?php echo URL;?>images/download.png" title="Download"></a>
				</tr> 
                <?php } ?>
			</tbody> 
			</table>
			</div>            
            
		</div><!-- end of .tab_container -->
		
		</article><!-- end of content manager article -->



			<script>
				$(document).ready(function() {
					$('#selecctall2').click(function(event) {  //on click
						if(this.checked) { // check select status
							$('.select2').each(function() { //loop through each checkbox
								this.checked = true;  //select all checkboxes with class "checkbox1"              
							});
						}else{
							$('.select2').each(function() { //loop through each checkbox
								this.checked = false; //deselect all checkboxes with class "checkbox1"                      
							});        
						}
					});
					
					$('#selecctall3').click(function(event) {  //on click
						if(this.checked) { // check select status
							$('.select3').each(function() { //loop through each checkbox
								this.checked = true;  //select all checkboxes with class "checkbox1"              
							});
						}else{
							$('.select3').each(function() { //loop through each checkbox
								this.checked = false; //deselect all checkboxes with class "checkbox1"                      
							});        
						}
					});
					
					
					$('#selecctall4').click(function(event) {  //on click
						if(this.checked) { // check select status
							$('.select4').each(function() { //loop through each checkbox
								this.checked = true;  //select all checkboxes with class "checkbox1"              
							});
						}else{
							$('.select4').each(function() { //loop through each checkbox
								this.checked = false; //deselect all checkboxes with class "checkbox1"                      
							});        
						}
					});
					
					
					$('#selecctall5').click(function(event) {  //on click
						if(this.checked) { // check select status
							$('.select5').each(function() { //loop through each checkbox
								this.checked = true;  //select all checkboxes with class "checkbox1"              
							});
						}else{
							$('.select5').each(function() { //loop through each checkbox
								this.checked = false; //deselect all checkboxes with class "checkbox1"                      
							});        
						}
					});
					
				});
            </script>


