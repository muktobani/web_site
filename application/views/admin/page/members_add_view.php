   <?php echo validation_errors('<h4 class="alert_warning">','</h4>'); ?>
    <article class="module width_full">
      <script src="<?php echo JS;?>calender/8_jquery-1.12.4.js"></script>
      <script src="<?php echo JS;?>calender/9_jquery-ui.js"></script>
       <?php $action=($data_arr[0]['member_id'])?"update":"save";?>
       <?php echo form_open_multipart(URL.'members/'.$action);?>
       
        <header><h3>New Member</h3></header>
            <div class="module_content">
                <fieldset>
                	
                    <h2>&nbsp; To become BAB member you:</h2>
                    <strong>
                    &nbsp; * Must be over 18 years old.<br />
                    &nbsp; * Must be Bangladeshi by born/Souse/parents.<br />
                    &nbsp; * Must live in Queensland.<br />
                    &nbsp; * Must be Australian Citizen/parmanent Resident/Holds Valid VISA.<br />
                    <span style="color:#F00;">&nbsp; * Required Fields.</span>
                    </strong>
                </fieldset>
                <fieldset>
    				<table border="0" width="98%" cellpadding="3">
                    	<tr>
                        	<td align="right"><strong>Title : </strong></td>
                        	<td><?php echo form_dropdown('cbo_title', $name_title_arr,set_value('cbo_title',$data_arr[0]['title']));?></td>
                            <td><span>*</span></td>
                    	</tr>
                    	<tr>
                        	<td align="right"><strong>First Name : </strong></td>
                        	<td><?php echo form_input('cbo_first_name',set_value('cbo_first_name',$data_arr[0]['first_name']));?></td>
                            <td><span>*</span></td>
                            
                        	<td align="right"><strong>Middle Name : </strong></td>
                        	<td><?php echo form_input('txt_middle_name',set_value('txt_middle_name',$data_arr[0]['middle_name']));?></td>
                            <td></td>
                        	<td align="right"><strong>Surname : </strong></td>
                        	<td><?php echo form_input('txt_last_name',set_value('txt_last_name',$data_arr[0]['last_name']));?></td>
                            <td><span>*</span></td>
                    	</tr>
                          <script>
						  $( function() {
							$( "#txt_birth_date" ).datepicker();
						  } );
						  </script>

                      
                    	<tr>
                        	<td align="right"><strong>Nick Name/Known as : </strong></td>
                        	<td><?php echo form_input('txt_nick_name',set_value('txt_nick_name',$data_arr[0]['nick_name']));?></td>
                            <td></td>
                        	<td align="right"><strong>Date of Birth : </strong></td>
                        	<td><?php 
								$birth_date_input = array(
									'name' => 'txt_birth_date',
									'id' => 'txt_birth_date',
									'value' => set_value('txt_birth_date',$data_arr[0]['birth_date']),
									'placeholder' => 'YYYY/mm/dd'
								);
								echo form_input($birth_date_input);
							?></td>
                            <td></td>
                    	</tr>
                      
                    	<tr>
                        	<td align="right"><strong>Home Phone : </strong></td>
                        	<td><?php echo form_input('txt_phone',set_value('txt_phone',$data_arr[0]['phone']));?></td>
                            <td></td>
                        	<td align="right"><strong>Mobile : </strong></td>
                        	<td><?php echo form_input('txt_mobile',set_value('txt_mobile',$data_arr[0]['mobile']));?></td>
                            <td><span>*</span></td>
                        	<td align="right"><strong>Email : </strong></td>
                        	<td><?php echo form_input('txt_email',set_value('txt_email',$data_arr[0]['email']));?></td>
                            <td><span>*</span></td>
                    	</tr>
                      
                      
                    	<tr>
                        	<td align="right"><strong>Address : </strong></td>
                    	</tr>
                      
                    	<tr>
                        	<td align="right"><strong>House No: </strong></td>
                        	<td><?php echo form_input('txt_house_no',set_value('txt_house_no',$data_arr[0]['house_no']));?></td>
                            <td><span>*</span></td>
                        	<td align="right"><strong>Street Name : </strong></td>
                        	<td><?php echo form_input('txt_street_name',set_value('txt_street_name',$data_arr[0]['street_name']));?></td>
                            <td><span>*</span></td>
                        	<td align="right"><strong>Suburb : </strong></td>
                        	<td><?php echo form_input('txt_city',set_value('txt_city',$data_arr[0]['city']));?></td>
                            <td><span>*</span></td>
                    	</tr>
                    	<tr>
                        	<td align="right"><strong>Post Code : </strong></td>
                            <td><?php  echo form_input('txt_zip',set_value('txt_zip',($data_arr[0]['zip'])?$data_arr[0]['zip']:'QLD - '));?></td>
                    	</tr>
                        
                        
                    	<tr>
                        	<td align="right"><strong>Refrred by : </strong></td>
                        	<td colspan="6"><b>(Refree must need to be current BAB member)</b></td>
                    	</tr>
                    	
                    	<tr>
                        	<td align="right"><strong>Your Blood Group : </strong></td>
                        	<td><?php echo form_dropdown('cbo_blood_group', $blood_group_arr,set_value('cbo_blood_group',$data_arr[0]['blood_group']));?></td>
                             <td><span>*</span></td>
                    	</tr>
                    	
                    	
                        
                    	<tr>
                        	<td colspan="2"><strong>Should BAB contact you for Blood donation? : </strong><span>*</span></td>
                    	</tr>
                        <tr>
                        	<td align="right"></td>
                        	<td>
								<?php 
                                foreach($yes_no_arr as $key=>$yes_no){
                                    echo form_radio('txt_is_blood_donate', $key, NULL, 'id="txt_is_blood_donate" '.set_radio('txt_is_blood_donate', $data_arr[0]['is_blood_donate'],($data_arr[0]['is_blood_donate']==$key)?true:false)).$yes_no;
                                }
                                ?>
                            </td>
                    	</tr>
                    	
                    	
                    	<tr>
                        	<td colspan="4">
                            <strong>Do you want to be BAB Valunteer? : </strong>
                            (If select yes, yes will be contract to asist when needed)<span>*</span>
                            </td>
                    	</tr>
                        <tr>
                        	<td></td>
                        	<td valign="top">
								<?php 
                                foreach($yes_no_arr as $key=>$yes_no){
                                    echo form_radio('txt_is_valunteer', $key, NULL, 'id="txt_is_valunteer" '.set_radio('txt_is_valunteer', $data_arr[0]['is_valunteer'],($data_arr[0]['is_valunteer']==$key)?true:false)).$yes_no;
                                }
                                ?>
                            </td>
                    	</tr>
                    	
                        
                        
                        
                        <tr>
                        	<td align="right"><strong>Full Name : </strong></td>
                        	<td><?php echo form_input('txt_ref_name',set_value('txt_ref_name',$data_arr[0]['ref_name']));?></td>
                            <td><span>*</span></td>
                        	<td align="right"><strong>Mobile : </strong></td>
                        	<td><?php echo form_input('txt_ref_mobile',set_value('txt_ref_mobile',$data_arr[0]['ref_mobile']));?></td>
                            <td><span>*</span></td>
                    	</tr>
                        
                        
                    	<tr>
                        	<td align="right"><strong>Select Membership Type : </strong></td>
                            <td><span>*</span></td>
                    	</tr>
                        
                        
                    	<tr>
                        	<td></td>
                        	<td colspan="6">
                            <?php 
							foreach($member_type_arr as $key=>$member_type){
								echo form_radio('txt_member_type', $key, NULL, 'id="txt_member_type" '.set_radio('txt_member_type', $data_arr[0]['member_type'],($data_arr[0]['member_type']==$key)?true:false)).$member_type.'<br>';
							}
							?>
                            </td>
                    	</tr>
                        
                        
                    	<tr>
                        	<td align="right"><strong>Please Select Membership Terms & Fees : </strong></td>
                            <td>
                            <?php echo form_dropdown('cbo_valid_year_fees', $valid_year_fees_arr,set_value('cbo_valid_year_fees',$data_arr[0]['valid_year']));?></td>
                            <td><span>*</span></td>
                    	</tr>
                        
                    	<tr>
                        	<td align="right"><strong>Profile Photo : </strong></td>
                            <td colspan="5">&nbsp;&nbsp;<input type="file" name="member_photo[]" /></td>
                    	</tr>
                        
                        
                     	<tr>
                            <td colspan="7">
                            	<strong>
                                Please pay the selected membership fees in blow account to complete
                                your membership application. Plz email payment receipt to process it faster.
                                </strong>
                             </td>
                    	</tr>
                       
                        
                        
                     	<tr>
                        	<td></td>
                            <td colspan="5">
                            	<strong>
                                Account Name: Bangladesh Association Brisbane<br>
                                BSB : 064131 Acc Number : 10009853<br>
                                Payment Description/Reference: Your 1st Name<br>
                                Email receipt at: accounts@bab@org.au
                                </strong>
                             </td>
                    	</tr>
                       
                       
                     	<tr>
                        	<td></td>
                            <td colspan="7">
                            	<strong>
                                <?php echo form_checkbox('txt_is_agree',1,set_checkbox('txt_is_agree',$data_arr[0]['is_agree'],($data_arr[0]['is_agree'])?true:false));?>
                                I agree to accept rules of the Association and regulations (constituation) of the Bangladesh Association in Brisbane.<span>*</span>
                                </strong>
                             </td>
                    	</tr>
                       
                       
                        
                    </table>
                </fieldset>

            </div>
        <footer>
            <div class="submit_link">
                 <?php echo form_dropdown('cbo_status', $status_arr,set_value('cbo_status',$data_arr[0]['is_active']));?>
                <input type="submit" value="<?php echo ucfirst($action); ?>" class="alt_btn">
                <input type="reset" value="Reset">
                <?php echo form_hidden('update_id',set_value('update_id',$data_arr[0]['member_id'])) ?>
            </div>
        </footer>
        
      <?php echo form_close(); ?>  
    </article><!-- end of post new article -->
