<h4 class="button_bar"><a href="<?php echo URL;?>ad_banner/add" class="href_btn">Add New</a></h4>
		
		<article class="module width_4_quarter">
		<header><h3 class="tabs_involved">Ad Banner Manager</h3>
		<ul class="tabs">
   			<li><a href="#tab1">Ad Banner</a></li>
		</ul>
		</header>




		<div class="tab_container">
			<div id="tab1" class="tab_content">
			<table class="tablesorter" cellspacing="0"> 
			<thead> 
				<tr> 
   					<th>#</th> 
    				<th>Banner</th> 
    				<th>Page</th> 
    				<th>Position</th> 
    				<th>Post Date</th> 
    				<th>Actions</th> 
				</tr> 
			</thead> 
			<tbody> 
				<?php foreach($this->my_model->sqlQuery('ad_banner','','','50') as $rows){?>
                <tr> 
   					<td width="15"><input type="checkbox" name="select[]"></td> 
    				<td width="46"> <img src="<?php echo URL;?>uploads/ad/<?php echo @$rows['banner_photo'];?>" width="45"></td> 
    				<td><?php echo $page_arr[$rows['view_page']];?></td> 
    				<td><?php echo $position_arr[$rows['ad_position']];?></td> 
    				<td><?php echo $rows['insert_time'];?></td> 
    				<td width="60">
                    <a href="<?php echo URL;?>ad_banner/add/<?php echo $rows['ad_banner_id'];?>"><input type="image" src="<?php echo URL;?>images/icn_edit.png" title="Edit"></a>
                    <a href="javascript:confirm_delete('<?php echo URL.'ad_banner/delete/'.$rows[ad_banner_id];?>')"><input type="image" src="<?php echo URL;?>images/icn_trash.png" title="Trash"></a>
                    </td> 
				</tr> 
                <?php } ?>
			</tbody> 
			</table>
			</div><!-- end of #tab1 -->
			
            
		</div><!-- end of .tab_container -->
		
		</article><!-- end of content manager article -->
