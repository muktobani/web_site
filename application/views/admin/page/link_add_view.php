   <?php echo validation_errors('<h4 class="alert_warning">','</h4>'); ?>
    <article class="module width_full">
       <?php $action=($data_arr[0]['media_link_id'])?"update":"save";?>
       <?php echo form_open_multipart(URL.'link/'.$action);?>
       
        <header><h3>Post New Link</h3></header>
            <div class="module_content">
                    <fieldset>
                        <label>Icon </label>
                        <input type="file" name="txt_icon[]" />
                    </fieldset>

                    <fieldset>
                        <label>Title </label>
                        <?php echo form_input('txt_title',  set_value('txt_title',$data_arr[0]['title']));?>
                    </fieldset>

                    <fieldset>
                        <label>Link</label>
                        <?php echo form_input('txt_link',  set_value('txt_link',$data_arr[0]['link']));?>
                    </fieldset>

                    <fieldset>
                        <label>Category</label>
                        <?php echo form_dropdown('cbo_category', $link_category,set_value('cbo_category',$data_arr[0]['category']));?>
                    </fieldset>
                    
            </div>
        <footer>
            <div class="submit_link">
                 <?php echo form_dropdown('cbo_status', $status_arr,set_value('cbo_status',$data_arr[0]['is_active']));?>
                <input type="submit" value="<?php echo ucfirst($action); ?>" class="alt_btn">
                <input type="reset" value="Reset">
                <?php echo form_hidden('update_id',set_value('update_id',$data_arr[0]['media_link_id'])) ?>
            </div>
        </footer>
        
      <?php echo form_close(); ?>  
    </article><!-- end of post new article -->
