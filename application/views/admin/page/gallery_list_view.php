<h4 class="button_bar"><a href="<?php echo URL;?>gallery/add" class="href_btn">Add New</a></h4>
		
		<article class="module width_4_quarter">
		<header><h3 class="tabs_involved">Gallery Manager</h3>
		<ul class="tabs">
   			<li><a href="#tab1">Gallery</a></li>
		</ul>
		</header>




		<div class="tab_container">
			<div id="tab1" class="tab_content">
			<table class="tablesorter" cellspacing="0"> 
			<thead> 
				<tr> 
   					<th>#</th> 
    				<th>Icon</th> 
    				<th>Title</th> 
    				<th>Album</th> 
    				<th>Post Date</th> 
    				<th>Actions</th> 
				</tr> 
			</thead> 
			<tbody> 
				<?php 
				//sqlQuery($table,$column,$where,$limit)
				foreach($this->my_model->sqlQuery('lib_gallery','','','') as $rows){?>
                <tr> 
   					<td width="15"><input type="checkbox" name="select[]"></td> 
    				<td width="46"> <img src="<?php echo URL;?>uploads/gallery/<?php echo @$rows['photo'];?>" width="45"></td> 
    				<td><?php echo $rows['gallery_title'];?></td> 
    				<td><?php echo $album_arr[$rows['album_id']];?></td> 
    				<td><?php echo $rows['insert_time'];?></td> 
    				<td width="60">
                    <a href="<?php echo URL;?>gallery/add/<?php echo $rows['gallery_id'];?>"><input type="image" src="<?php echo URL;?>images/icn_edit.png" title="Edit"></a>
                    <a href="javascript:confirm_delete('<?php echo URL.'gallery/delete/'.$rows[gallery_id];?>')"><input type="image" src="<?php echo URL;?>images/icn_trash.png" title="Trash"></a>
                    </td> 
				</tr> 
                <?php } ?>
			</tbody> 
			</table>
			</div><!-- end of #tab1 -->
			
			
            
			            
            
		</div><!-- end of .tab_container -->
		
		</article><!-- end of content manager article -->
