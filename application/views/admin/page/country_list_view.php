<h4 class="button_bar"><a href="<?php echo URL;?>country/add" class="href_btn">Add New</a></h4>
		
		<article class="module width_4_quarter">
		<header><h3 class="tabs_involved">Country Manager</h3>
		<ul class="tabs">
   			<li><a href="#tab1">Country</a></li>
		</ul>
		</header>




		<div class="tab_container">
			<div id="tab1" class="tab_content">
			<table class="tablesorter" cellspacing="0"> 
			<thead> 
				<tr> 
   					<th>#</th> 
    				<th>Flag</th> 
    				<th>Country Name</th> 
    				<th>Short Name</th> 
    				<th>Currency</th> 
    				<th>Country Code</th> 
    				<th>Actions</th> 
				</tr> 
			</thead> 
			<tbody> 
				<?php 
				//sqlQuery($table,$column,$where,$limit)
				foreach($this->my_model->sqlQuery('lib_country','','','') as $rows){?>
                <tr> 
   					<td width="15"><input type="checkbox" name="select[]"></td> 
    				<td width="46"> <img src="<?php echo URL;?>uploads/flag/<?php echo @$rows['flag'];?>" width="45"></td> 
    				<td><?php echo $rows['country_full_name'];?></td> 
    				<td><?php echo $rows['country_short_name'];?></td> 
    				<td><?php echo $rows['currency'];?></td> 
    				<td><?php echo $rows['country_code'];?></td> 
    				<td width="60">
                    <a href="<?php echo URL;?>country/add/<?php echo $rows['country_id'];?>"><input type="image" src="<?php echo URL;?>images/icn_edit.png" title="Edit"></a>
                    <a href="javascript:confirm_delete('<?php echo URL.'country/delete/'.$rows[country_id];?>')"><input type="image" src="<?php echo URL;?>images/icn_trash.png" title="Trash"></a>
                    </td> 
				</tr> 
                <?php } ?>
			</tbody> 
			</table>
			</div><!-- end of #tab1 -->
			
			
            
			            
            
		</div><!-- end of .tab_container -->
		
		</article><!-- end of content manager article -->
