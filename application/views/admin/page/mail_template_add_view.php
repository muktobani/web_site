   <?php echo validation_errors('<h4 class="alert_warning">','</h4>'); ?>
    <article class="module width_full">
       <?php $action=($data_arr[0]['mail_template_id'])?"update":"save";?>
       <?php echo form_open_multipart(URL.'mail_template/'.$action);?>
       
        <header><h3>Post New Article</h3></header>
            <div class="module_content">
                    <fieldset>
                        <label>Mail Template Name</label>
                        <?php echo form_input('txt_mail_template_name',  set_value('txt_mail_template_name',$data_arr[0]['mail_template_name']));?>
                    </fieldset>
                    
                    <fieldset>
                        <label>Mail Subject</label>
                        <?php echo form_input('txt_mail_subject',  set_value('txt_mail_subject',$data_arr[0]['mail_subject']));?>
                    </fieldset>
					
                    <fieldset>
                    <b> &nbsp;&nbsp; MAIL TEMPLATE</b>
						<textarea id="txt_mail_template" name="txt_mail_template"><?php echo set_value('txt_mail_template',$data_arr[0]['mail_template']);?></textarea>
						<?php echo display_ckeditor($ckeditor); ?>
                    </fieldset>
                   
            </div>
        <footer>
            <div class="submit_link">
                <?php echo form_dropdown('cbo_status', $status_arr,set_value('cbo_status',$data_arr[0]['is_active']));?>
                <input type="submit" value="<?php echo ucfirst($action); ?>" class="alt_btn">
                <input type="reset" value="Reset">
                <?php echo form_hidden('update_id',set_value('update_id',$data_arr[0]['mail_template_id'])) ?>
            </div>
        </footer>
        
      <?php echo form_close(); ?>  
    </article><!-- end of post new article -->
