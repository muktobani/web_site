   <?php echo validation_errors('<h4 class="alert_warning">','</h4>'); ?>
    <article class="module width_full">
       <?php $action=($data_arr[0]['ad_banner_id'])?"update":"save";?>
       <?php echo form_open_multipart(URL.'ad_banner/'.$action);?>
       
        <header><h3>Post New Ad Banner</h3></header>
            <div class="module_content">
                    <fieldset>
                        <label>Banner Image</label>
                        <input type="file" name="txt_ad_banner_photo[]" />
                    </fieldset>

                    <fieldset>
                        <label>View Page</label>
                        <?php echo form_dropdown('txt_ad_banner_view_page', $page_arr,set_value('txt_ad_banner_view_page',$data_arr[0]['view_page']));?>
                    </fieldset>

                    <fieldset>
                        <label>Ad Position</label>
                        <?php echo form_dropdown('txt_ad_position', $position_arr,set_value('txt_ad_position',$data_arr[0]['ad_position']));?>
                    </fieldset>

                    
            </div>
        <footer>
            <div class="submit_link">
                 <?php echo form_dropdown('cbo_status', $status_arr,set_value('cbo_status',$data_arr[0]['is_active']));?>
                <input type="submit" value="<?php echo ucfirst($action); ?>" class="alt_btn">
                <input type="reset" value="Reset">
                <?php echo form_hidden('update_id',set_value('update_id',$data_arr[0]['ad_banner_id'])) ?>
            </div>
        </footer>
        
      <?php echo form_close(); ?>  
    </article><!-- end of post new article -->
