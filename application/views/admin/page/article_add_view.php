   <?php echo validation_errors('<h4 class="alert_warning">','</h4>'); ?>
    <article class="module width_full">
       <?php $action=($data_arr[0]['article_id'])?"update":"save";?>
       <?php echo form_open_multipart(URL.'article/'.$action);?>
       
        <header><h3>Post New Article</h3></header>
            <div class="module_content">
                    <fieldset>
                        <label>Category</label>
                        <?php echo form_dropdown('cbo_category', $article_category,set_value('cbo_category',$data_arr[0]['category_id']));?>
                    </fieldset>
                    
                    <fieldset>
                        <label>Photo </label>
                        <input type="file" name="txt_article_photo[]" />
                    </fieldset>

                    <fieldset>
                        <label>Article Title</label>
                        <?php echo form_input('txt_article_title',  set_value('txt_article_title',$data_arr[0]['title']));?>
                    </fieldset>
					
                    <fieldset>
                    <b> &nbsp;&nbsp; DESCRIPTION</b>
						<textarea id="txt_article" name="txt_article"><?php echo set_value('txt_article',$data_arr[0]['article']);?></textarea>
						<?php echo display_ckeditor($ckeditor); ?>
                    </fieldset>
            </div>
        <footer>
            <div class="submit_link">
                 <?php echo form_dropdown('cbo_status', $status_arr,set_value('cbo_status',$data_arr[0]['is_active']));?>
                <input type="submit" value="<?php echo ucfirst($action); ?>" class="alt_btn">
                <input type="reset" value="Reset">
                <?php echo form_hidden('update_id',set_value('update_id',$data_arr[0]['article_id'])) ?>
            </div>
        </footer>
        
      <?php echo form_close(); ?>  
    </article><!-- end of post new article -->
