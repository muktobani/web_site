   <?php echo validation_errors('<h4 class="alert_warning">','</h4>'); ?>
    <article class="module width_full">
       <?php $action=($data_arr[0]['country_id'])?"update":"save";?>
       <?php echo form_open_multipart(URL.'country/'.$action);?>
       
        <header><h3>Post New Article</h3></header>
            <div class="module_content">
                    <fieldset>
                        <label>Flag</label>
                        <input type="file" name="txt_flag[]" />
                    </fieldset>

                    <fieldset>
                        <label>Countery Full Name </label>
                        <?php echo form_input('txt_country_full_name',  set_value('txt_country_full_name',$data_arr[0]['country_full_name']));?>
                    </fieldset>

                    <fieldset>
                        <label>Countery Short Name </label>
                        <?php echo form_input('txt_country_short_name',  set_value('txt_country_short_name',$data_arr[0]['country_short_name']));?>
                    </fieldset>

                    <fieldset>
                        <label>Currency</label>
                        <?php echo form_input('txt_currency',  set_value('txt_currency',$data_arr[0]['currency']));?>
                    </fieldset>

                    <fieldset>
                        <label>Countery Code</label>
                        <?php echo form_input('txt_country_code',  set_value('txt_country_code',$data_arr[0]['country_code']));?>
                    </fieldset>

                    
            </div>
        <footer>
            <div class="submit_link">
                 <?php echo form_dropdown('cbo_status', $status_arr,set_value('cbo_status',$data_arr[0]['is_active']));?>
                <input type="submit" value="<?php echo ucfirst($action); ?>" class="alt_btn">
                <input type="reset" value="Reset">
                <?php echo form_hidden('update_id',set_value('update_id',$data_arr[0]['country_id'])) ?>
            </div>
        </footer>
        
      <?php echo form_close(); ?>  
    </article><!-- end of post new article -->
