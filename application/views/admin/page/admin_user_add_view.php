   <?php echo validation_errors('<h4 class="alert_warning">','</h4>'); ?>
    <article class="module width_full">
       <?php $action=($data_arr[0]['member_id'])?"update":"save";?>
       <?php echo form_open_multipart(URL.'admin_user/'.$action);?>
       
        <header><h3>Post New Admin</h3></header>
            <div class="module_content">
                    <fieldset>
                        <label>Photo </label>
                        <input type="file" name="txt_photo[]" />
                    </fieldset>

                    <fieldset>
                        <label>User Id</label>
                        <?php echo form_input('txt_user_id',  set_value('txt_user_id',$data_arr[0]['user_id']));?>
                    </fieldset>

                    <fieldset>
                        <label>User Name</label>
                        <?php echo form_input('txt_user_name',  set_value('txt_user_name',$data_arr[0]['first_name']));?>
                    </fieldset>

                    <fieldset>
                        <label>Password</label>
                        <?php echo form_password('cbo_password',set_value('cbo_password'));?>
                    </fieldset>
                    <fieldset>
                        <label>Re Password</label>
                        <?php echo form_password('cbo_repassword',set_value('cbo_repassword'));?>
                    </fieldset>
                    
            </div>
        <footer>
            <div class="submit_link">
                <?php echo form_dropdown('cbo_status', $status_arr,set_value('cbo_status',$data_arr[0]['is_active']));?>
                <input type="submit" value="<?php echo ucfirst($action); ?>" class="alt_btn">
                <input type="reset" value="Reset">
                <?php echo form_hidden('update_id',set_value('update_id',$data_arr[0]['member_id'])) ?>
            </div>
        </footer>
        
      <?php echo form_close(); ?>  
    </article><!-- end of post new article -->
