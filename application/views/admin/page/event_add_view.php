  <script src="<?php echo JS;?>calender/8_jquery-1.12.4.js"></script>
  <script src="<?php echo JS;?>calender/9_jquery-ui.js"></script>
 
   <?php echo validation_errors('<h4 class="alert_warning">','</h4>'); ?>
    <article class="module width_full">
       <?php $action=($data_arr[0]['event_id'])?"update":"save";?>
       <?php echo form_open_multipart(URL.'event/'.$action);?>
       
        <header><h3>Post New Event</h3></header>
            <div class="module_content">
                    <fieldset>
                        <label>Date</label>
                       	<?php 
							$birth_date_input = array(
								'name' => 'txt_event_date',
								'id' => 'txt_event_date',
								'value' => set_value('txt_event_date',$data_arr[0]['event_date']),
								'placeholder' => 'YYYY/mm/dd'
							);
							echo form_input($birth_date_input);
						?>
                    </fieldset>
                    <fieldset>
                        <label>Time</label>
                        <?php echo form_input('txt_event_time',  set_value('txt_event_time',$data_arr[0]['event_time']));?>
                    </fieldset>
                    
                    <fieldset>
                        <label>Photo </label>
                        <input type="file" name="txt_article_photo[]" />
                    </fieldset>

                    <fieldset>
                        <label>Vinue</label>
                        <?php echo form_input('txt_vinue',  set_value('txt_vinue',$data_arr[0]['vinue']));?>
                    </fieldset>
                  
                    <fieldset>
                        <label>Title</label>
                        <?php echo form_input('txt_title',  set_value('txt_title',$data_arr[0]['title']));?>
                    </fieldset>
                  
                    <fieldset>
                    <b> &nbsp;&nbsp; DESCRIPTION</b>
						<textarea id="txt_event" name="txt_event"><?php echo set_value('txt_article',$data_arr[0]['article']);?></textarea>
						<?php echo display_ckeditor($ckeditor); ?>
                    </fieldset>
                    
                    <fieldset>
                    <label>Gallery</label>
                        <table id="fabrication_table">
                            <tbody>
                                <tr>
                                    <td><input type="file" multiple="" id="txtfabricationA_1" name="txt_gallery[]" /></td>
                                    <td>
                                        <input type="button" id="increasefabrication_1" style="width:30px" class="formbutton" value="+" onClick="add_break_down_tr1(this.id,'fabrication_table')" />
                                        <input type="button" id="decreasefabrication_1" style="width:30px" class="formbutton" value="-" onClick="delete_break_down_tr1(this.id,'fabrication_table')"/>
                                    </td>
                                </tr>
                            </tbody>
                        </table>                    
                    </fieldset>
                    
                    
                    
            </div>
        <footer>
            <div class="submit_link">
                 <?php echo form_dropdown('cbo_status', $status_arr,set_value('cbo_status',$data_arr[0]['is_active']));?>
                <input type="submit" value="<?php echo ucfirst($action); ?>" class="alt_btn">
                <input type="reset" value="Reset">
                <?php echo form_hidden('update_id',set_value('update_id',$data_arr[0]['article_id'])) ?>
            </div>
        </footer>
        
      <?php echo form_close(); ?>  
    </article><!-- end of post new article -->


  <script>
  $( function() {
	$("#txt_event_date").datepicker({ dateFormat: 'yy/mm/dd'});
  } );
  </script>

