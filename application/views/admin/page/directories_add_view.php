   <?php echo validation_errors('<h4 class="alert_warning">','</h4>'); ?>
    <article class="module width_full">
       <?php $action=($data_arr[0]['directories_id'])?"update":"save";?>
       <?php echo form_open_multipart(URL.'directories/'.$action);?>
       
        <header><h3>Post New Article</h3></header>
            <div class="module_content">
                    <fieldset>
                        <label>Logo</label>
                        <input type="file" name="txt_logo[]" />
                    </fieldset>

                    <fieldset>
                        <label>Company Name </label>
                        <?php echo form_input('txt_company_name',  set_value('txt_company_name',$data_arr[0]['company_name']));?>
                    </fieldset>

                    <fieldset>
                        <label>Web Address</label>
                        <?php echo form_input('txt_web_address',  set_value('txt_web_address',$data_arr[0]['web_address']));?>
                    </fieldset>

                    <fieldset>
                        <label>Email Address</label>
                        <?php echo form_input('txt_email_address',  set_value('txt_email_address',$data_arr[0]['email_address']));?>
                    </fieldset>


                    <fieldset>
                        <label>Phone Number</label>
                        <?php echo form_input('txt_phone',  set_value('txt_phone',$data_arr[0]['phone']));?>
                    </fieldset>


                    <fieldset>
                        <label>Description</label>
                        <?php echo form_textarea('txt_description',  set_value('txt_description',$data_arr[0]['description']));?>
                    </fieldset>


                    <fieldset>
                        <label>Google Map</label>
                        <?php echo form_textarea('txt_google_map',  set_value('txt_google_map',$data_arr[0]['google_map']));?>
                    </fieldset>



                    <fieldset>
                        <label>Business Category</label>
                        <?php echo form_dropdown('cbo_business_category', $company_category,set_value('cbo_business_category',$data_arr[0]['business_category']));?>
                    </fieldset>
                    
                    
                    <fieldset>
                        <label>Facebook</label>
                        <?php echo form_input('txt_facebook',set_value('txt_facebook',$data_arr[0]['facebook']));?>
                    </fieldset>
                    
                    <fieldset>
                        <label>Google plus</label>
                        <?php echo form_input('txt_google_plus',set_value('txt_google_plus',$data_arr[0]['google_plus']));?>
                    </fieldset>
                    
                    <fieldset>
                        <label>Linkedin</label>
                        <?php echo form_input('txt_linkedin',set_value('txt_linkedin',$data_arr[0]['linkedin']));?>
                    </fieldset>
                    
                    <fieldset>
                        <label>Twitter</label>
                        <?php echo form_input('txt_twitter',set_value('txt_twitter',$data_arr[0]['twitter']));?>
                    </fieldset>
                    
                    
                    <fieldset>
                    <label>Gallery Photo</label>
                        <table id="fabrication_table">
                            <tbody>
                                <tr>
                                    <td><input type="file" multiple="" id="txtfabricationA_1" name="txt_gallery[]" /></td>
                                    <td>
                                        <input type="button" id="increasefabrication_1" style="width:30px" class="formbutton" value="+" onClick="add_break_down_tr1(this.id,'fabrication_table')" />
                                        <input type="button" id="decreasefabrication_1" style="width:30px" class="formbutton" value="-" onClick="delete_break_down_tr1(this.id,'fabrication_table')"/>
                                    </td>
                                </tr>
                            </tbody>
                        </table>                    
                    </fieldset>
                    
            </div>
        <footer>
            <div class="submit_link">
                 <?php echo form_dropdown('cbo_status', $status_arr,set_value('cbo_status',$data_arr[0]['is_active']));?>
                <input type="submit" value="<?php echo ucfirst($action); ?>" class="alt_btn">
                <input type="reset" value="Reset">
                <?php echo form_hidden('update_id',set_value('update_id',$data_arr[0]['directories_id'])) ?>
            </div>
        </footer>
        
      <?php echo form_close(); ?>  
    </article><!-- end of post new article -->
