   <?php echo validation_errors('<h4 class="alert_warning">','</h4>'); ?>
    <article class="module width_full">
       <?php $action=($data_arr[0]['id'])?"update":"save";?>
       <?php echo form_open_multipart(URL.'fees/'.$action);?>
       
        <header><h3>Add New Fees</h3></header>
            <div class="module_content">
                    <fieldset>
                        <label>Renew Year</label>
                        <?php echo form_dropdown('txt_renew_year', $renew_year_arr,set_value('txt_renew_year',$data_arr[0]['renew_year']));?>
                    </fieldset>

                    <fieldset>
                        <label>Fees</label>
                         <?php echo form_input('txt_fees',  set_value('txt_fees',$data_arr[0]['fees']));?>
                    </fieldset>

                    
            </div>
        <footer>
            <div class="submit_link">
                 <?php echo form_dropdown('cbo_status', $status_arr,set_value('cbo_status',$data_arr[0]['is_active']));?>
                <input type="submit" value="<?php echo ucfirst($action); ?>" class="alt_btn">
                <input type="reset" value="Reset">
                <?php echo form_hidden('update_id',set_value('update_id',$data_arr[0]['id'])) ?>
            </div>
        </footer>
        
      <?php echo form_close(); ?>  
    </article><!-- end of post new article -->
