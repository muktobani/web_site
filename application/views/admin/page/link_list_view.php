<h4 class="button_bar"><a href="<?php echo URL;?>link/add" class="href_btn">Add New</a></h4>
		
		<article class="module width_4_quarter">
		<header><h3 class="tabs_involved">Link Manager</h3>
		<ul class="tabs">
   			<li><a href="#tab1">Social Media</a></li>
    		<li><a href="#tab2">Web</a></li>
   			<li><a href="#tab3">Video</a></li>
		</ul>
		</header>

		<div class="tab_container">
			<div id="tab1" class="tab_content">
			<table class="tablesorter" cellspacing="0"> 
			<thead> 
				<tr> 
   					<th>#</th> 
    				<th>Icon</th> 
    				<th>Title</th> 
    				<th>Link</th> 
    				<th>Post Date</th> 
    				<th>Actions</th> 
				</tr> 
			</thead> 
			<tbody> 
				<?php foreach($this->my_model->get_media_link('category',2) as $rows){?>
                <tr> 
   					<td width="15"><input type="checkbox" name="select[]"></td> 
    				<td width="46"> <img src="<?php echo URL;?>uploads/icon/<?php echo @$rows['link_icon'];?>" width="45"></td> 
    				<td><?php echo $rows['title'];?></td> 
    				<td><?php echo $rows['link'];?></td> 
    				<td><?php echo $rows['insert_time'];?></td> 
    				<td width="60">
                    <a href="<?php echo URL;?>link/add/<?php echo $rows['media_link_id'];?>"><input type="image" src="<?php echo URL;?>images/icn_edit.png" title="Edit"></a>
                    <a href="javascript:confirm_delete('<?php echo URL.'link/delete/'.$rows[media_link_id];?>')"><input type="image" src="<?php echo URL;?>images/icn_trash.png" title="Trash"></a>
                    </td> 
				</tr> 
                <?php } ?>
			</tbody> 
			</table>
			</div><!-- end of #tab1 -->
			
			<div id="tab2" class="tab_content">
			<table class="tablesorter" cellspacing="0"> 
			<thead> 
				<tr> 
   					<th>#</th> 
    				<th>Icon</th> 
    				<th>Title</th> 
    				<th>Link</th> 
    				<th>Post Date</th> 
    				<th>Actions</th> 
				</tr> 
			</thead> 
			<tbody> 
				<?php foreach($this->my_model->get_media_link('category',1) as $rows){?>
                <tr> 
   					<td width="15"><input type="checkbox" name="select[]"></td> 
    				<td width="46"><img src="<?php echo URL;?>uploads/icon/<?php echo @$rows['link_icon'];?>" width="45"></td> 
    				<td><?php echo $rows['title'];?></td> 
    				<td><?php echo $rows['link'];?></td> 
    				<td><?php echo $rows['insert_time'];?></td> 
    				<td width="60">
                    <a href="<?php echo URL;?>link/add/<?php echo $rows['media_link_id'];?>"><input type="image" src="<?php echo URL;?>images/icn_edit.png" title="Edit"></a>
                    <a href="javascript:confirm_delete('<?php echo URL.'link/delete/'.$rows[media_link_id];?>')"><input type="image" src="<?php echo URL;?>images/icn_trash.png" title="Trash"></a>
                    </td> 
				</tr> 
                <?php } ?>
			</tbody> 
			</table>

			</div><!-- end of #tab2 -->
			
            
			<div id="tab3" class="tab_content">
			<table class="tablesorter" cellspacing="0"> 
			<thead> 
				<tr> 
   					<th>#</th> 
    				<th>Icon</th> 
    				<th>Title</th> 
    				<th>Link</th> 
    				<th>Post Date</th> 
    				<th>Actions</th> 
				</tr> 
			</thead> 
			<tbody> 
				<?php foreach($this->my_model->get_media_link('category',3) as $rows){?>
                <tr> 
   					<td width="15"><input type="checkbox" name="select[]"></td> 
    				<td width="46"> <img src="<?php echo URL;?>uploads/icon/<?php echo @$rows['link_icon'];?>" width="45"></td> 
    				<td><?php echo $rows['title'];?></td> 
    				<td><?php echo $rows['link'];?></td> 
    				<td><?php echo $rows['insert_time'];?></td> 
    				<td width="60">
                    <a href="<?php echo URL;?>link/add/<?php echo $rows['media_link_id'];?>"><input type="image" src="<?php echo URL;?>images/icn_edit.png" title="Edit"></a>
                    <a href="javascript:confirm_delete('<?php echo URL.'link/delete/'.$rows[media_link_id];?>')"><input type="image" src="<?php echo URL;?>images/icn_trash.png" title="Trash"></a>
                    </td> 
				</tr> 
                <?php } ?>
			</tbody> 
			</table>

			</div>            
            
		</div><!-- end of .tab_container -->
		
		</article><!-- end of content manager article -->
