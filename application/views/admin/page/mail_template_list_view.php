	<?php echo $this->session->flashdata('msg'); ?>
    
    <h4 class="button_bar"><a href="<?php echo URL;?>mail_template/add" class="href_btn">Add New</a></h4>
		
		<article class="module width_4_quarter">
		<header><h3 class="tabs_involved">Mail Template Manager</h3>
		<ul class="tabs">
   			<li><a href="#tab1">Mail Template</a></li>
		</ul>
		</header>

		<div class="tab_container">
			<div id="tab1" class="tab_content">
			<table class="tablesorter" cellspacing="0"> 
			<thead> 
				<tr> 
   					<th>#</th> 
    				<th>Template Name</th> 
    				<th>Mail Subject</th> 
    				<th>Post Date</th> 
    				<th>Status</th> 
    				<th>Actions</th> 
				</tr> 
			</thead> 
			<tbody> 
				<?php foreach($this->my_model->sqlQuery('mail_template','','','50') as $rows){?>
                <tr> 
   					<td width="15"><input type="checkbox" name="select[]"></td> 
    				<td><?php echo $rows['mail_template_name'];?></td> 
    				<td><?php echo $rows['mail_subject'];?></td> 
    				<td><?php echo $rows['insert_time'];?></td> 
    				<td><?php echo $status_arr[$rows['is_active']];?></td> 
    				<td width="90">
                   
                   <a href="<?php echo URL;?>mail_template/send/<?php echo $rows['mail_template_id'];?>"><input type="image" src="<?php echo URL;?>images/send.png" title="Send"></a>
                    <a href="<?php echo URL;?>mail_template/add/<?php echo $rows['mail_template_id'];?>"><input type="image" src="<?php echo URL;?>images/icn_edit.png" title="Edit"></a>
                    <a href="javascript:confirm_delete('<?php echo URL.'mail_template/delete/'.$rows[mail_template_id];?>')"><input type="image" src="<?php echo URL;?>images/icn_trash.png" title="Trash"></a>
                    </td> 
				</tr> 
                <?php } ?>
			</tbody> 
			</table>
			</div><!-- end of #tab1 -->
			
            
		</div><!-- end of .tab_container -->
		
		</article><!-- end of content manager article -->
