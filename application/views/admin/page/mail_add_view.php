   <?php echo validation_errors('<h4 class="alert_warning">','</h4>'); ?>
    <?php echo $this->session->flashdata('msg'); ?>
    <article class="module width_full">
      
       <?php $action=($data_arr[0]['mail_id'])?"send":"send";?>
       <?php echo form_open_multipart(URL.'mail/'.$action);?>
       
        <header><h3>	Send Message to BAB</h3></header>
            <div class="module_content">
                <fieldset>
    				
                   
                    <table border="0" width="95%" cellpadding="3">
                        <tr>
                        	<td width="130" align="right"><strong>Send To : </strong></td>
                        	<td><?php echo form_input('txt_send_to',set_value('txt_send_to',$data_arr[0]['send_to']));?></td>
                        </tr>
                        <tr>
                        	<td width="130" align="right"><strong>Subject : </strong></td>
                        	<td><?php echo form_input('txt_subject',set_value('txt_subject',$data_arr[0]['mail_subject']));?></td>
                        </tr>
                    	<tr>
                        	<td align="right"><strong>Message : </strong></td>
                        	<td><textarea id="txt_message" name="txt_message"><?php echo set_value('txt_message',$data_arr[0]['mail_body']);?></textarea>
</td>
                        </tr>
                        
                    	<tr>
                        	<td align="right"><strong>Attach File : </strong></td>
                            <td>&nbsp;&nbsp;&nbsp;<input type="file" name="txt_attach_file[]" /></td>
                    	</tr>  
                        
                        
                        
                        
                    </table>
                </fieldset>

            </div>
        <footer>
            <div class="submit_link">
                 <?php echo form_dropdown('cbo_status', $status_arr,set_value('cbo_status',1));?>
                <input type="submit" value="<?php echo ucfirst($action); ?>" class="alt_btn">
                <input type="reset" value="Reset">
                <?php echo form_hidden('update_id',set_value('update_id',$data_arr[0]['member_id'])) ?>
            </div>
        </footer>
        
      <?php echo form_close(); ?>  
    </article><!-- end of post new article -->
