
		<form class="quick_search">
			<input type="text" placeholder="Quick Search" value="">
		</form>
		<hr/>
		<h3>Content</h3>
		<ul class="toggle">
			<li class="icn_new_article"><a href="<?php echo URL;?>link">Link</a></li>
			<li class="icn_new_article"><a href="<?php echo URL;?>directories">Directory</a></li>
			<li class="icn_new_article"><a href="<?php echo URL;?>members">Members</a></li>
			<li class="icn_new_article"><a href="<?php echo URL;?>article">Article</a></li>
            <li class="icn_new_article"><a href="<?php echo URL;?>ad_banner">Ad Banner</a></li>
            <li class="icn_new_article"><a href="<?php echo URL;?>event">Event</a></li>
		</ul>
		<h3>Users</h3>
		<ul class="toggle">
			<li class="icn_add_user"><a href="<?php echo URL;?>admin_user/add">Add New User</a></li>
			<li class="icn_view_users"><a href="<?php echo URL;?>admin_user">View Users</a></li>
			<li class="icn_profile"><a href="<?php echo URL;?>">Your Profile</a></li>
			<li class="icn_jump_back"><a href="<?php echo URL;?>login/log_out">Logout</a></li>
		</ul>
		<h3>Media</h3>
		<ul class="toggle">
			<!--<li class="icn_folder"><a href="#">File Manager</a></li>-->
			<li class="icn_photo"><a href="<?php echo URL;?>gallery">Gallery</a></li>
			<li class="icn_folder"><a href="<?php echo URL;?>mail">Mail Send</a></li>
            <!--<li class="icn_audio"><a href="#">Audio</a></li>
			<li class="icn_video"><a href="#">Video</a></li>-->
		</ul>
		<h3>Library</h3>
		<ul class="toggle">
			<li class="icn_settings"><a href="<?php echo URL;?>album">Album</a></li>
			<li class="icn_security"><a href="<?php echo URL;?>country">Country</a></li>
			<li class="icn_security"><a href="<?php echo URL;?>mail_template">Mail Template</a></li>
			<li class="icn_security"><a href="<?php echo URL;?>fees">Renew Year & Fees</a></li>
		</ul>
		
		<footer>
			<hr />
			<p><strong>Copyright &copy; 2011 Website Admin</strong></p>
			<p>Theme by <a href="http://www.medialoot.com">MediaLoot</a></p>
		</footer>
	