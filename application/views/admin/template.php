<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//define............
$logged_in =$this->session->userdata('logged_in');
$photo =$this->session->userdata('photo');
$username=$this->session->userdata('username');
if($logged_in!=TRUE){redirect('admin/login');exit('No direct script access allowed');}
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <title><?php echo $title;?></title>
    <meta name="description" content="<?php echo $description;?>">
    <meta name="keywords" content="<?php echo $keywords;?>">
    <meta name="author" content="PHP Boss">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="favicon.gif" type="image/x-icon" />  
	 
	 <?php   
        foreach (glob("css/*.css") as $cssFile) {
		    echo'<link rel="stylesheet" href="'.URL.$cssFile.'">';
        }

        foreach (glob("js/*.js") as $jsFile) {
		    echo'<script src="'.URL.$jsFile.'"></script>';
        }
     ?> 
     
     
</head>
<body>

	<header id="header">
		<hgroup>
			<h1 class="site_title"><a href="index.html">Website Admin</a></h1>
			<h2 class="section_title">Dashboard</h2>
            <div class="btn_view_site"><a href="<?php echo URL;?>">View Site</a></div>
		</hgroup>
	</header> <!-- end of header bar -->

	<section id="secondary_bar">
		<div class="user">
			<p><?php echo $username;?><a href="#"></a></p>
			<a class="logout_user" href="<?php echo URL;?>login/log_out" title="Logout">Logout</a> 
		</div>
		<div class="breadcrumbs_container">
			<article class="breadcrumbs">
                <a href="javascript:toggle('sidebar')">||||</a>
                <div class="breadcrumb_divider"></div> 
                <a href="<?php echo URL;?>admin/dashboard">Dashboard</a>
                <div class="breadcrumb_divider"></div> 
                <a class="current"><?php echo $current_page;?></a>
                </article>
		</div>
	</section><!-- end of secondary bar -->

	<aside id="sidebar" class="column" style="display:block;"><?php $this->load->view('admin/menu');?></aside>
    <section id="main" class="column"><?php $this->load->view($container);?></section>

</div>
	 
<?php if($photo){?>     
<style>
.user p {
	background: url(<?php echo URL.'uploads/admin/'.$photo;?>) no-repeat center left;
	background-size: 20px 20px;
	background-repeat: no-repeat;
}
</style>
<?php } f?>
</body>
</html>
