   <?php echo validation_errors('<h4 class="alert_warning">','</h4>'); ?>
    <article class="module width_full">
       <?php $action=($data_arr[0]['member_id'])?"update":"save";?>
       <?php echo form_open_multipart(URL.'members/'.$action);?>
       
        <header><h3>Member Reg. Form</h3></header>
            <div class="module_content">
                <fieldset>
    				<table border="0" width="95%" cellpadding="3">
                    	<tr>
                        	<td align="right"><strong>Title : </strong></td>
                        	<td><?php echo form_dropdown('cbo_first_name', $first_name_arr,set_value('cbo_first_name',$data_arr[0]['first_name']));?></td>
                            <td><span>*</span></td>
                    	</tr>
                    	<tr>
                        	<td align="right"><strong>First Name : </strong></td>
                        	<td><?php echo form_input('txt_middle_name',set_value('txt_middle_name',$data_arr[0]['middle_name']));?></td>
                            <td><span>*</span></td>
                            
                        	<td align="right"><strong>Middle Name : </strong></td>
                        	<td><?php echo form_input('txt_last_name',set_value('txt_last_name',$data_arr[0]['last_name']));?></td>
                            <td></td>
                        	<td align="right"><strong>Username : </strong></td>
                        	<td><?php echo form_input('txt_last_name',set_value('txt_last_name',$data_arr[0]['last_name']));?></td>
                            <td><span>*</span></td>
                    	</tr>
                        
                      
                    	<tr>
                        	<td align="right"><strong>Nick Name/Know as : </strong></td>
                        	<td><?php echo form_input('txt_nick_name',set_value('txt_nick_name',$data_arr[0]['nick_name']));?></td>
                            <td></td>
                        	<td align="right"><strong>Date of Birth : </strong></td>
                        	<td><?php echo form_input('txt_birth_date',set_value('txt_birth_date',$data_arr[0]['birth_date']));?></td>
                            <td></td>
                    	</tr>
                      
                      
                    	<tr>
                        	<td align="right"><strong>Home Phone : </strong></td>
                        	<td><?php echo form_input('txt_phone',set_value('txt_phone',$data_arr[0]['phone']));?></td>
                            <td></td>
                        	<td align="right"><strong>Mobile : </strong></td>
                        	<td><?php echo form_input('txt_mobile',set_value('txt_mobile',$data_arr[0]['mobile']));?></td>
                            <td><span>*</span></td>
                        	<td align="right"><strong>Email : </strong></td>
                        	<td><?php echo form_input('txt_email',set_value('txt_email',$data_arr[0]['email']));?></td>
                            <td><span>*</span></td>
                    	</tr>
                      
                      
                    	<tr>
                        	<td align="right"><strong>Address : </strong></td>
                    	</tr>
                      
                    	<tr>
                        	<td align="right"><strong>House : </strong></td>
                        	<td><?php echo form_input('txt_phone',set_value('txt_phone',$data_arr[0]['phone']));?></td>
                            <td></td>
                        	<td align="right"><strong>Street Name : </strong></td>
                        	<td><?php echo form_input('txt_mobile',set_value('txt_mobile',$data_arr[0]['mobile']));?></td>
                            <td><span>*</span></td>
                        	<td align="right"><strong>Suburb : </strong></td>
                        	<td><?php echo form_input('txt_email',set_value('txt_email',$data_arr[0]['email']));?></td>
                            <td><span>*</span></td>
                    	</tr>
                    	<tr>
                        	<td align="right"><strong>Post Code : </strong></td>
                            <td><?php echo form_input('txt_email',set_value('txt_email',$data_arr[0]['email']));?></td>
                    	</tr>
                        
                        
                    	<tr>
                        	<td align="right"><strong>Refrred by : </strong></td>
                        	<td colspan="6"><b>(Refree must need to be current BAB member)</b></td>
                    	</tr>
                    	<tr>
                        	<td align="right"><strong>Full Name : </strong></td>
                        	<td><?php echo form_input('txt_phone',set_value('txt_phone',$data_arr[0]['phone']));?></td>
                            <td></td>
                        	<td align="right"><strong>Mobile : </strong></td>
                        	<td><?php echo form_input('txt_mobile',set_value('txt_mobile',$data_arr[0]['mobile']));?></td>
                            <td><span>*</span></td>
                    	</tr>
                        
                        
                    	<tr>
                        	<td><strong>Select Membership Type : </strong></td>
                            <td><span>*</span></td>
                    	</tr>
                        
                        
                    	<tr>
                        	<td></td>
                        	<td colspan="6">
                            <?php 
							foreach($member_type_arr as $key=>$member_type){
								echo form_radio('txt_member_type',set_value('txt_member_type',$key)).$member_type.'<br>';
							}
							?>
                            </td>
                    	</tr>
                        
                        
                    	<tr>
                        	<td colspan="2"><strong>Please Select Membership Terms & Fees : </strong><span>*</span></td>
                    	</tr>
                    	<tr>
                        	<td></td>
                            <td colspan="5">
                            <?php echo form_dropdown('cbo_first_name', $valid_year_fees_arr,set_value('cbo_first_name',$data_arr[0]['first_name']));?></td>
                    	</tr>
                        
                    	<tr>
                        	<td align="right"><strong>Photo : </strong></td>
                            <td colspan="5">&nbsp;&nbsp;<input type="file" name="member_photo[]" /></td>
                    	</tr>
                        
                        
                    </table>
                </fieldset>

            </div>
        <footer>
            <div class="submit_link">
                 <?php echo form_dropdown('cbo_status', $status_arr,set_value('cbo_status',$data_arr[0]['is_active']));?>
                <input type="submit" value="<?php echo ucfirst($action); ?>" class="alt_btn">
                <input type="reset" value="Reset">
                <?php echo form_hidden('update_id',set_value('update_id',$data_arr[0]['member_id'])) ?>
            </div>
        </footer>
        
      <?php echo form_close(); ?>  
    </article><!-- end of post new article -->
