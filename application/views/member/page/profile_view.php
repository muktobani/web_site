   <?php echo validation_errors('<h4 class="alert_warning">','</h4>'); ?>
    <article class="module width_full">
      
       <?php $action=($data_arr[0]['member_id'])?"update":"save";?>
       <?php echo form_open_multipart(URL.'profile/'.$action);?>
       
        <header><h3>My Profile</h3></header>
            <div class="module_content">
             <fieldset>
                <table width="95%">
                
                <tr><td valign="top">
    				<table border="0" width="95%" cellpadding="3">
                    	<tr>
                        	<td><h3>Welcome <?php echo $data_arr[0]['title'].'. '.$data_arr[0]['nick_name'];?></h3></td>
                    	</tr>
                        
                    	<tr>
                        	<td>
                            Today is <?php echo date("l, d M Y",time());?><br />
                            Your ar BAB Member since <?php echo date("M, Y",strtotime($data_arr[0]['approved_date']));?><br />
                            Your Membership expired on <?php echo date("M, Y",strtotime($data_arr[0]['expair_date']));?>

                            </td>
                    	</tr>
                        
                    </table>
				</td><td align="right">
                <fieldset style="width:25%; text-align:center"><img src="<?php echo URL.'/uploads/member/'.$data_arr[0]['photo'];?>" width="100" /></fieldset>
                <a href="<?php echo URL;?>profile/add">Update profile</a>
                <fieldset style="width:50%; text-align:center">
                    Your Membership ID: <?php echo $data_arr[0]['user_id'];?><br />
                    Last Logged in: <?php echo date("d M Y, h:m A",strtotime($data_arr[0]['approved_date']));?>
                </fieldset>
				</td></tr>
				<tr><td colspan="2">
                
                	<h2>Personal Information:</h2>
                    <hr/>
                    <table cellpadding="5">
                    	<tr>
                        	<td><strong>Full Name</strong></td><th>:</th>
                        	<td><?php echo $first_name_arr[$data_arr[0]['title']].' '.$data_arr[0]['first_name'].' '.$data_arr[0]['middle_name'].' '.$data_arr[0]['last_name'];?></td>
                        </tr>
                    
                    	<tr>
                        	<td><strong>Knickname/Known as</strong></td><th>:</th>
                        	<td><?php echo $data_arr[0]['nick_name'];?></td>
                        </tr>
                    
                    	<tr>
                        	<td><strong>Date of Birth</strong></td><th>:</th>
                        	<td><?php echo date("d M Y, h:m A",strtotime($data_arr[0]['birth_date']));?></td>
                        </tr>
                    
                    	<tr>
                        	<td><strong>Home Phone</strong></td><th>:</th>
                        	<td><?php echo $data_arr[0]['phone'];?></td>
                        </tr>
                    
                    	<tr>
                        	<td><strong>Mobile</strong></td><th>:</th>
                        	<td><?php echo $data_arr[0]['mobile'];?></td>
                        </tr>
                    
                    	<tr>
                        	<td><strong>Email</strong></td><th>:</th>
                        	<td><?php echo $data_arr[0]['email'];?></td>
                        </tr>
                    
                    	<tr>
                        	<td><strong>Address</strong></td><th>:</th>
                        	<td><?php echo $data_arr[0]['street_name'].', '.$data_arr[0]['city'].', '.$data_arr[0]['state'];?></td>
                        </tr>
                    
                    	<tr>
                        	<td><strong>Reffered by</strong></td><th>:</th>
                        	<td><?php echo $data_arr[0]['ref_name'];?></td>
                        </tr>
                    
                    	<tr>
                        	<td><strong>Membership type</strong></td><th>:</th>
                        	<td><?php echo $member_type_arr[$data_arr[0]['member_type']];?></td>
                        </tr>
                    	<tr>
                        	<td><strong>Membership Terms</strong></td><th>:</th>
                        	<td><?php echo $valid_year_arr[$data_arr[0]['valid_year']];?></td>
                        </tr>
                    	<tr>
                        	<td><strong>Blood Group</strong></td><th>:</th>
                        	<td><?php echo $blood_group_arr[$data_arr[0]['valid_year']];?></td>
                        </tr>
                    
                    </table>
                
				</td></tr>
                </table>
               </fieldset>
                
                
            </div>
        <footer>
            <div class="submit_link">
                <?php echo form_hidden('update_id',set_value('update_id',$data_arr[0]['member_id'])) ?>
            </div>
        </footer>
        
      <?php echo form_close(); ?>  
    </article><!-- end of post new article -->
