   <?php echo validation_errors('<h4 class="alert_warning">','</h4>'); ?>
    <?php echo $this->session->flashdata('msg'); ?>
    <article class="module width_full">
      
       <?php $action=($data_arr[0]['mail_id'])?"renew":"send";?>
       <?php echo form_open_multipart(URL.'member_mail/'.$action);?>
       
        <header><h3>Mail Details</h3></header>
            <div class="module_content">
                <fieldset>
    				
                   
                    <table width="95%" cellpadding="3" border="0">
                        <tr>
                        	<td width="35" rowspan="3" valign="top"><img width="35" src="<?php echo URL.'uploads/member/'.$this->session->userdata('photo');?>" /></td>
                            <td width="60" align="right"><strong>Send By : </strong></td>
                        	<td>
								<?php echo $username.' &lt;'.$data_arr[0]['send_by'].'&gt;';?>
                            	[ <a href="<?php echo URL;?>member_mail/add/<?php echo $data_arr[0]['mail_id'];?>">Send</a> ]
                            </td>
                        </tr>
                        <tr>
                        	<td align="right"><strong>Subject : </strong></td>
                        	<td><?php echo $data_arr[0]['mail_subject'];?></td>
                        </tr>
                        <tr>
                        	<td align="right"><strong>Message : </strong></td>
                        	<td><?php echo $data_arr[0]['mail_body'];?></td>
                        </tr>
                        
                    </table>
                </fieldset>

            </div>
        <footer>
            <div class="submit_link">
            </div>
        </footer>
        
      <?php echo form_close(); ?>  
    </article><!-- end of post new article -->
