   <?php echo validation_errors('<h4 class="alert_warning">','</h4>'); ?>
    <article class="module width_full">
      
       <?php $action=($data_arr[0]['member_id'])?"change":"save";?>
       <?php echo form_open_multipart(URL.'profile/'.$action);?>
       
        <header><h3>Password Change Form</h3></header>
            <div class="module_content">
                <fieldset>
    				<table border="0" width="55%" cellpadding="3">
                    	<tr>
                        	<td align="right"><strong>Current Password : </strong></td>
                        	<td><?php echo form_password('txt_password',set_value('txt_password'));?></td>
                            </tr>
                            <tr>
                            
                        	<td align="right"><strong>New Password : </strong></td>
                        	<td><?php echo form_password('txt_new_password',set_value('txt_new_password'));?></td>
                            </tr>
                            <tr>

                            <td align="right"><strong>Confirm Password : </strong></td>
                        	<td><?php echo form_password('txt_conf_password',set_value('txt_conf_password'));?></td>
                    	</tr>
                        
                        
                    </table>
                </fieldset>

            </div>
        <footer>
            <div class="submit_link">
                <input type="submit" value="<?php echo ucfirst($action); ?>" class="alt_btn">
                <input type="reset" value="Reset">
                <?php echo form_hidden('update_id',set_value('update_id',$data_arr[0]['member_id'])) ?>
            </div>
        </footer>
        
      <?php echo form_close(); ?>  
    </article><!-- end of post new article -->
